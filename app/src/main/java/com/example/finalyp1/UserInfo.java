package com.example.finalyp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class UserInfo extends AppCompatActivity {

    private FirebaseUser mUser;
    private int customerId;
    private double monthlyIncome;
    private String demo;
    private String accountType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        EditText editName = (EditText) findViewById(R.id.utilName);
        EditText editAddress1 = (EditText) findViewById(R.id.editUsersPostalAddress1);
        EditText editAddress2 = (EditText) findViewById(R.id.editUsersPostalAddress2);
        EditText editPhone = (EditText) findViewById(R.id.editUsersPhone);
        EditText editIncome = (EditText) findViewById(R.id.editUsersIncome);
        EditText editEircode = (EditText) findViewById(R.id.editUsersEircode);

        Spinner dropdown = findViewById(R.id.editCountySpinner);

        Intent intent = getIntent();
        accountType = intent.getStringExtra("type");

        Button useCaseButton = findViewById(R.id.button7);
        if(accountType.equals("Business")){
            useCaseButton.setVisibility(View.INVISIBLE);
        }

        String[] counties = new String[]{"Antrim", "Armagh","Carlow","Cavan","Clare","Cork","Derry","Donegal","Down","Dublin","Fermanagh","Galway",
                "Kerry","Kildare","Kilkenny","Laois","Leitrim","Limerick","Longford","Louth","Mayo","Meath","Monaghan","Offaly","Roscommon","Sligo",
                "Tipperary","Tyrone","Waterford","Westmeath","Wexford","Wicklow" };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, counties);

        dropdown.setAdapter(adapter);


        mUser = intent.getParcelableExtra("User");
        String userName = intent.getStringExtra("Name");
        String address1 = intent.getStringExtra("Address1");
        String address2 = intent.getStringExtra("Address2");
        String county = intent.getStringExtra("County");
        String eircode = intent.getStringExtra("Eircode");
        demo = intent.getStringExtra("Demographic");

        String phone = intent.getStringExtra("Phone");
        String income = String.valueOf(intent.getDoubleExtra("Income",0.0));
        customerId = intent.getIntExtra("id", 0);
        monthlyIncome = intent.getDoubleExtra("Income",0.0);


        dropdown.setSelection(getIndex(dropdown, county));
        editName.setText(userName);


        editEircode.setText(eircode);

        editAddress1.setText(address1);
        editAddress2.setText(address2);


        editPhone.setText(phone);

        editIncome.setText(income);

        ImageView imageView = (ImageView) findViewById(R.id.imageView3);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.edit_info);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);

    }

    public void updateCustomer(View view){

        EditText editName = (EditText) findViewById(R.id.utilName);
        EditText editAddress1 = (EditText) findViewById(R.id.editUsersPostalAddress1);
        EditText editAddress2 = (EditText) findViewById(R.id.editUsersPostalAddress2);
        EditText editPhone = (EditText) findViewById(R.id.editUsersPhone);
        EditText editIncome = (EditText) findViewById(R.id.editUsersIncome);
        EditText editEircode = (EditText) findViewById(R.id.editUsersEircode);

        Spinner dropdown = findViewById(R.id.editCountySpinner);
        double income = Double.parseDouble(editIncome.getText().toString());

        if(editName.getText().toString().equals(null)||editName.getText().toString().equals("")||
                editEircode.getText().toString().equals(null)||editEircode.getText().toString().equals("")||
                editAddress1.getText().toString().equals(null)||editAddress1.getText().toString().equals("")||
                editAddress2.getText().toString().equals(null)||editAddress2.getText().toString().equals("")||
                editPhone.getText().toString().equals(null)||editPhone.getText().toString().equals("")||
                editIncome.getText().toString().equals(null)||editIncome.getText().toString().equals("")){

            Toast.makeText(getApplicationContext(), "Enter Valid Details!", Toast.LENGTH_SHORT).show();

        }
        else {


            Customer updated = new Customer(editName.getText().toString(), mUser.getEmail(), editAddress1.getText().toString(), editAddress2.getText().toString(), dropdown.getSelectedItem().toString(), editEircode.getText().toString(), demo, editPhone.getText().toString(), income, accountType);


            ObjectMapper mapper = new ObjectMapper();
            //Converting the Object to JSONString
            String jsonString = null;
            try {
                jsonString = mapper.writeValueAsString(updated);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ;

            JSONObject json = null;
            try {
                json = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Jersey PUT with customer JSON and Users ID;

            RequestQueue queue = Volley.newRequestQueue(UserInfo.this);

            int id = customerId;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.PUT, "http://localhost:8080/FYP1/rest/customerwebservice/update/" + id, json, new
                    Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {


                            try {
                                String name = response.getString("name");
                                Log.i("Success", name + " updates in DB");
                                Toast.makeText(getApplicationContext(), "Your Information Has Been Updated!", Toast.LENGTH_SHORT).show();
                                editAddress1.setText(updated.getAddress1());
                                editIncome.setText(String.valueOf(updated.getMonthlyIncome()));
                                editName.setText(updated.getName());
                                editPhone.setText(updated.getPhoneNo());

                                if (accountType.equals("Customer")) {
                                    Intent intent = new Intent(getApplicationContext(), Settings.class);

                                    intent.putExtra("User", mUser);

                                    intent.putExtra("Income", monthlyIncome);

                                    intent.putExtra("id", customerId);

                                    intent.putExtra("type", accountType);

                                    startActivity(intent);
                                }

                                if (accountType.equals("Business")) {
                                    Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                                    intent.putExtra("User", mUser);

                                    intent.putExtra("Income", monthlyIncome);

                                    intent.putExtra("id", customerId);

                                    intent.putExtra("type", accountType);

                                    startActivity(intent);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {


                    Log.i("DB Failed", error.getMessage());

                }


            }
            );

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(jsonObjectRequest);

        }
    }

    public void useCase(View view){

        Intent i = new Intent(getApplicationContext(), UseCaseQuiz.class);

        i.putExtra("id", customerId);
        i.putExtra("User", mUser);
        i.putExtra("Income", monthlyIncome);
        i.putExtra("type", accountType);

        startActivity(i);


    }

    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }
}