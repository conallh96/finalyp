package com.example.finalyp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Settings extends AppCompatActivity {

    private FirebaseUser mUser;
    private int userId;
    private String usersUtilites;
    private String usersProviders;
    private double monthlyIncome;
    private String accountType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        monthlyIncome = intent.getDoubleExtra("Income", 0.0);
        accountType = intent.getStringExtra("type");

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url = "http://localhost:8080/FYP1/rest/customerutilwebservice/myutils/" + userId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                usersUtilites = response;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.logout){

            Intent intent = new Intent(this, Login.class);

            startActivity(intent);

        }

        if(item.getItemId()==R.id.home){

            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }



    public void pressInfo(View view) {

        Intent i = new Intent(getApplicationContext(), UserInfo.class);


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, "http://localhost:8080/FYP1/rest/customerwebservice/customerEmail/" + mUser.getEmail(), null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {

                            Log.i("Response", response.toString());

                            String name = response.getString("name");
                            String address1 = response.getString("address1");
                            String address2 = response.getString("address2");
                            String eircode = response.getString("eircode");
                            String county = response.getString("county");
                            String phoneNo = response.getString("phoneNo");
                            String demo = response.getString("demographic");
                            monthlyIncome = Double.parseDouble(response.getString("monthlyIncome"));



                            i.putExtra("Name", name);
                            i.putExtra("Address1", address1);
                            i.putExtra("Address2", address2);
                            i.putExtra("Eircode", eircode);
                            i.putExtra("County", county);
                            i.putExtra("Phone", phoneNo);
                            i.putExtra("id", userId);
                            i.putExtra("User", mUser);
                            i.putExtra("Income", monthlyIncome);
                            i.putExtra("Demographic", demo);
                            i.putExtra("type", accountType);
                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Customer Details: ", "Failure");

            }


        }
        );

        queue.add(jsonObjectRequest);


    }

    public void pressEditSubs(View view) {


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url1 = "http://localhost:8080/FYP1/rest/customerutilwebservice/myproviders/" + userId;
        StringRequest stringRequest1 = new StringRequest(Request.Method.GET, url1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                usersProviders = response;

                Intent i = new Intent(getApplicationContext(), EditMySubs.class);
                i.putExtra("User", mUser);
                i.putExtra("Utilities", usersUtilites);
                i.putExtra("Providers", usersProviders);
                i.putExtra("Income", monthlyIncome);
                i.putExtra("type", accountType);

                i.putExtra("id", userId);
                startActivity(i);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest1);


    }

}