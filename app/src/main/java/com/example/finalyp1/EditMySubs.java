package com.example.finalyp1;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class EditMySubs extends AppCompatActivity implements EditDialog.EditDialogListener, EditCustomerUtilDialog.EditCustomerUtilDialogListener {

    ArrayList<Utility> utilities = new ArrayList<>();
    ArrayList<String> providersList = new ArrayList<>();
    ArrayList<String> idsList = new ArrayList<>();
    private FirebaseUser mUser;
    private String accountType;
    private double monthlyIncome;
    private UtilityAdapter adapter = new UtilityAdapter(utilities, providersList);
    private int userId;
    private String[] systemCats;
    private String editedProv;
    private int editedProvId;
    private Utility editedUtility;
    private Utility sentUtility;
    private CustomerUtil editedCustUtil;
    private CustomerUtil sentCustUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_subs);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvUtils);
        new SystemCats().execute();


        Intent intent = getIntent();
        monthlyIncome = intent.getDoubleExtra("Income", 0.0);
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");
        String response = intent.getStringExtra("Utilities");
        String provResponse = intent.getStringExtra("Providers");
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        ImageView imageView = (ImageView) findViewById(R.id.imageView5);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.edit_subs);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);


        String[] utilityStrings = response.split("/");

        for (String utilityString : utilityStrings) {

            Log.i("Response Utility", utilityString);


            try {
                JSONObject json = new JSONObject(utilityString);

                Gson g = new Gson();
                Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);

                utilities.add(theUtil);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        String[] provStrings = provResponse.split("/");

        for (String provString : provStrings) {


            providersList.add(provString);


        }


        String url = "http://localhost:8080/FYP1/rest/customerutilwebservice/myutilsids/" + userId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String[] utilityIdsStrings = response.split("/");

                Collections.addAll(idsList, utilityIdsStrings);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);


        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(EditMySubs.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(EditMySubs.this, DividerItemDecoration.VERTICAL));
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {


            Utility edit = utilities.get(viewHolder.getAdapterPosition());

            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET, "http://localhost:8080/FYP1/rest/customerutilwebservice/utilId/" + idsList.get(viewHolder.getAdapterPosition()), null, new
                    Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {


                            Gson g = new Gson();
                            CustomerUtil theUtil = g.fromJson(String.valueOf(response), CustomerUtil.class);


                            if (edit.getType().equals("User")) {
                                openMyCustomDialog(theUtil, edit, systemCats);
                            } else {
                                openMyDialog(theUtil, edit);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.i("Get CustomerUtil", "Failure");

                }


            }
            );

            queue.add(jsonObjectRequest);


        }
    };

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            Utility utilView = utilities.get(viewHolder.getAdapterPosition());

            Log.i("Adapter Position", String.valueOf(viewHolder.getAdapterPosition()));

            AlertDialog.Builder builder1 = new AlertDialog.Builder(viewHolder.itemView.getContext());
            builder1.setTitle("Unsubscribe from " + utilView.getName());
            builder1.setMessage("Are you sure you want to unsubscribe from " + utilView.getName() + "?\n\n It we be removed from your profile however DigiTools" +
                    "hold no responsibility for cancelling agreements with Service Providers.\n\n");
            builder1.setCancelable(true);


            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();


                            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

                            String url1 = "http://localhost:8080/FYP1/rest/utilwebservice/removeId/" + utilities.get(viewHolder.getAdapterPosition()).getId();
                            StringRequest stringRequest1 = new StringRequest(Request.Method.DELETE, url1, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    Log.i("Successful Clear", response);

                                    adapter.notifyDataSetChanged();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    Log.i("DB Failed", error.getMessage());

                                }


                            });


                            //Volley Delete Customer Utility to DB

                            String url = "http://localhost:8080/FYP1/rest/customerutilwebservice/removeId/" + idsList.get(viewHolder.getAdapterPosition());
                            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    Log.i("Successful DELETE", response);
                                    Toast.makeText(getApplicationContext(), "Subscription Removed Successfully!", Toast.LENGTH_SHORT).show();


                                    if (utilView.getType().equals("User")) {
                                        queue.add(stringRequest1);
                                    }

                                    utilities.remove(viewHolder.getAdapterPosition());
                                    idsList.remove(viewHolder.getAdapterPosition());
                                    adapter.notifyDataSetChanged();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    Log.i("DB Failed", error.getMessage());

                                }


                            });

                            queue.add(stringRequest);


                            adapter.notifyDataSetChanged();

                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            adapter.notifyDataSetChanged();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();


        }
    };


    public void openMyDialog(CustomerUtil cUtil, Utility util) {
        EditDialog exampleDialog = new EditDialog(cUtil, util);
        exampleDialog.show(getSupportFragmentManager(), "Edit Utility Dialog");
    }

    public void openMyCustomDialog(CustomerUtil cUtil, Utility util, String[] systemCats) {
        EditCustomerUtilDialog exampleCustDialog = new EditCustomerUtilDialog(cUtil, util, systemCats);
        exampleCustDialog.show(getSupportFragmentManager(), "Edit Utility Dialog");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void applyTexts(CustomerUtil cUtil) {


        ObjectMapper mapper = new ObjectMapper();
        //Converting the Object to JSONString
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(cUtil);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;

        JSONObject json = null;
        try {
            json = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());


        //Volley Update Customer Utility to DB

        String url = "http://localhost:8080/FYP1/rest/customerutilwebservice/update/" + cUtil.getId();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT, url, json, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            String name = response.getString("name");
                            Log.i("Success", name + " updates in DB");
                            Toast.makeText(getApplicationContext(), "Subscription Edited Successfully!", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter.notifyDataSetChanged();

                        if (accountType.equals("Customer")) {
                            Intent intent = new Intent(getApplicationContext(), Settings.class);

                            intent.putExtra("User", mUser);

                            intent.putExtra("Income", monthlyIncome);

                            intent.putExtra("id", userId);

                            intent.putExtra("type",accountType);

                            startActivity(intent);
                        }

                        if (accountType.equals("Business")) {
                            Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                            intent.putExtra("User", mUser);

                            intent.putExtra("Income", monthlyIncome);

                            intent.putExtra("id", userId);

                            intent.putExtra("type",accountType);

                            startActivity(intent);
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Log.i("DB Failed", error.getMessage());

            }


        }
        );


        queue.add(jsonObjectRequest);


    }

    @Override
    public void updateRc() {

        adapter.notifyDataSetChanged();

    }

    @Override
    public void applyTexts(CustomerUtil cUtil, Utility util, String providerString) {

        editedCustUtil = cUtil;
        editedUtility = util;
        editedProv = providerString;
        Provider provider = new Provider(editedProv);
        sentCustUtil = cUtil;


        ObjectMapper mapper = new ObjectMapper();
        //Converting the Object to JSONString
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(provider);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;

        JSONObject jsonProvider = null;
        try {
            jsonProvider = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestQueue queue = Volley.newRequestQueue(EditMySubs.this);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, "http://localhost:8080/FYP1/rest/providerwebservice/post", jsonProvider, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                            try {
                                editedProvId = response.getInt("id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.i("Success", editedProvId + " is the Id of " + editedProv);


                            // Create Utility Object and Parse to Json
                            sentUtility = new Utility(editedUtility.getMonthlyPrice(), editedUtility.getName(), editedUtility.getDescription(), editedProvId, "User", editedUtility.getCategory());

                            sentUtility.setId(editedUtility.getId());

                            Log.i("Sent Utility", sentUtility.toString());
                            Log.i("Sent Cust Util", sentCustUtil.toString());
                            new CustomerUtilUpdate().execute();



                    }
                        },new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {


                                Log.i("DB Failed", error.getMessage());

                            }

        }
        );

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);

    }


    @Override
    public void updateRcv() {

        adapter.notifyDataSetChanged();

    }

    private class SystemCats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/systemcategories")
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                systemCats = response.body().string().split("/");


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {



        }
    }

    private class CustomerUtilUpdate extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Gson g = new Gson();
            // Check DB for provider


            String json = g.toJson(sentCustUtil);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, json);

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/customerutilwebservice/update/" + sentCustUtil.getId())
                    .put(body)
                    .build();

            String json1 = g.toJson(sentUtility);
            MediaType JSON1 = MediaType.parse("application/json; charset=utf-8");
            RequestBody body1 = RequestBody.create(JSON1, json1);

            okhttp3.Request request1 = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/update/" + sentUtility.getId())
                    .put(body1)
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                Log.i("CUtil Response", response.body().string());

                okhttp3.Response response1 = client.newCall(request1).execute();
                assert response1.body() != null;
                Log.i("Util Response", response1.body().string());



            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {


            Toast.makeText(getApplicationContext(), "Subscription Successfully Edited ", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), Home.class);

            intent.putExtra("User", mUser);

            intent.putExtra("Income", monthlyIncome);

            intent.putExtra("id", userId);

            intent.putExtra("type", accountType);

            startActivity(intent);

        }
    }
}
