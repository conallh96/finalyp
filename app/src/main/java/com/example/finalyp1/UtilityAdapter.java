package com.example.finalyp1;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

public class UtilityAdapter extends RecyclerView.Adapter<UtilityAdapter.ViewHolder>{
    private List<Utility> utils;
    private List<String> providers;
    long DURATION = 500;
    private boolean on_attach = true;
    private int selectedPos = RecyclerView.NO_POSITION;



    public UtilityAdapter(List<Utility> utils, ArrayList<String> providersList){
        this.utils = utils;
        this.providers = providersList;

    }

    @NonNull
    @Override
    public UtilityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.rcview_row, parent, false);


        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Utility util = utils.get(position);

        holder.itemView.setSelected(selectedPos == position);

        if(selectedPos == position)
            holder.itemView.setBackgroundColor(Color.GRAY);


        else
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);


        holder.name.setText(util.getName());
        String editDesc = "Description: " + util.getDescription();
        holder.description.setText(editDesc);
        String priceEdit = "€" + String.valueOf(util.getMonthlyPrice());
        holder.price.setText(priceEdit);
        String catEdit = "Category: " + util.getCategory();
        holder.category.setText(catEdit);
        String fromProv = "From: " + providers.get(position);
        holder.provider.setText(fromProv);

        setAnimation(holder.itemView, position);


    }

    @Override
    public int getItemCount() {
        return utils.size();
    }




    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 3 : (i * DURATION / 4));
        animator.setDuration(300);
        animatorSet.play(animator);
        animator.start();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public TextView description;
        public TextView provider;
        public TextView price;
        public TextView category;




        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            name = (TextView) itemView.findViewById(R.id.titleTxt);
            description = (TextView) itemView.findViewById(R.id.descTxt);
            price = (TextView) itemView.findViewById(R.id.utilityPrice);
            provider = (TextView) itemView.findViewById(R.id.sourceTxt);
            category = (TextView) itemView.findViewById(R.id.dateTxt);

        }





        @Override
        public void onClick(View v) {

            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);

            Utility current = utils.get(selectedPos);



            Log.i("Clicked", current.toString());
        }


    }

}
