package com.example.finalyp1;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.internal.http.RealResponseBody;

public class Home extends AppCompatActivity {

    private FirebaseUser mUser;
    private String usersUtilites;
    private String usersProviders;
    private ArrayList<NewsPiece> news = new ArrayList<>();
    private int userId;
    private int scroller = 0;
    private double monthlyIncome;
    private String accountType;
    private double currentLat = 0.0;
    private double currentLong = 0.0;
    private String iconString, weatherConditions, temperature, locality;



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        monthlyIncome = intent.getDoubleExtra("Income", 0.0);
        accountType = intent.getStringExtra("type");
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        currentLong = location.getLongitude();
        currentLat = location.getLatitude();




        new NewsGetter().execute();
        new WeatherGetter().execute();




        View divider1 = findViewById(R.id.divider1);
        divider1.setVisibility(View.INVISIBLE);


        String url = "http://localhost:8080/FYP1/rest/customerutilwebservice/myutils/" + userId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                usersUtilites = response;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //add option menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        inflater.inflate(R.menu.settings, menu);


        return true;
    }

    public void pressUtils(View view) {

        RequestQueue queue = Volley.newRequestQueue(this);

        //Need to change to String Request

        String url = "http://localhost:8080/FYP1/rest/utilwebservice/systemutilities";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Intent i = new Intent(getApplicationContext(), ViewDeals.class);
                i.putExtra("User", mUser);
                i.putExtra("Response", response);
                i.putExtra("id", userId);
                i.putExtra("Income", monthlyIncome);
                i.putExtra("type", accountType);
                startActivity(i);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);


    }

    public void pressInfo(View view) {

        Intent i = new Intent(getApplicationContext(), UserInfo.class);


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, "http://localhost:8080/FYP1/rest/customerwebservice/customerEmail/" + mUser.getEmail(), null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {

                            Log.i("Response", response.toString());

                            String name = response.getString("name");
                            String address1 = response.getString("address1");
                            String address2 = response.getString("address2");
                            String eircode = response.getString("eircode");
                            String county = response.getString("county");
                            String phoneNo = response.getString("phoneNo");
                            String demo = response.getString("demographic");



                            i.putExtra("Name", name);
                            i.putExtra("Address1", address1);
                            i.putExtra("Address2", address2);
                            i.putExtra("Eircode", eircode);
                            i.putExtra("County", county);
                            i.putExtra("Phone", phoneNo);
                            i.putExtra("id", userId);
                            i.putExtra("User", mUser);
                            i.putExtra("Income", monthlyIncome);
                            i.putExtra("Demographic", demo);
                            i.putExtra("type", accountType);
                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Customer Details: ", "Failure");

            }


        }
        );

        queue.add(jsonObjectRequest);


    }

    public void pressAdd(View view) {


        Intent i = new Intent(getApplicationContext(), AddUtil.class);


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, "http://localhost:8080/FYP1/rest/customerwebservice/customerEmail/" + mUser.getEmail(), null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {

                            Log.i("Response", response.toString());

                            String name = response.getString("name");

                            String phoneNo = response.getString("phoneNo");
                            String customerId = response.getString("id");


                            i.putExtra("Name", name);

                            i.putExtra("Phone", phoneNo);
                            i.putExtra("id", userId);
                            i.putExtra("User", mUser);
                            i.putExtra("Income", monthlyIncome);
                            i.putExtra("type", accountType);
                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Customer Details: ", "Failure");

            }


        }
        );

        queue.add(jsonObjectRequest);


    }

    public void pressView(View view) {


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url1 = "http://localhost:8080/FYP1/rest/customerutilwebservice/myproviders/" + userId;
        StringRequest stringRequest1 = new StringRequest(Request.Method.GET, url1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                usersProviders = response;

                Intent i = new Intent(getApplicationContext(), MyBreakdown.class);
                i.putExtra("User", mUser);
                i.putExtra("Utilities", usersUtilites);
                i.putExtra("Providers", usersProviders);
                i.putExtra("Income", monthlyIncome);
                i.putExtra("type", accountType);

                i.putExtra("id", userId);
                startActivity(i);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest1);

    }

    public void pressEditSubs(View view) {


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url1 = "http://localhost:8080/FYP1/rest/customerutilwebservice/myproviders/" + userId;
        StringRequest stringRequest1 = new StringRequest(Request.Method.GET, url1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                usersProviders = response;

                Intent i = new Intent(getApplicationContext(), EditMySubs.class);
                i.putExtra("User", mUser);
                i.putExtra("Utilities", usersUtilites);
                i.putExtra("Providers", usersProviders);
                i.putExtra("Income", monthlyIncome);
                i.putExtra("type", accountType);

                i.putExtra("id", userId);
                startActivity(i);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest1);


    }

    public void pressConnectionDetails(View view) {


        Intent i = new Intent(getApplicationContext(), ConnectionDetails.class);

        i.putExtra("id", userId);
        i.putExtra("User", mUser);
        i.putExtra("Income", monthlyIncome);
        i.putExtra("type", accountType);

        startActivity(i);


    }

    public void pressMapSearch(View view) {


        Intent i = new Intent(getApplicationContext(), MapSearch.class);

        i.putExtra("id", userId);
        i.putExtra("User", mUser);
        i.putExtra("Income", monthlyIncome);
        i.putExtra("type", accountType);

        startActivity(i);


    }

    public void pressChecker(View view) {


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url1 = "http://localhost:8080/FYP1/rest/customerutilwebservice/myproviders/" + userId;
        StringRequest stringRequest1 = new StringRequest(Request.Method.GET, url1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                usersProviders = response;

                Intent i = new Intent(getApplicationContext(), PriceChecker.class);
                i.putExtra("User", mUser);
                i.putExtra("Utilities", usersUtilites);
                i.putExtra("Providers", usersProviders);
                i.putExtra("Income", monthlyIncome);
                i.putExtra("type", accountType);

                i.putExtra("id", userId);
                startActivity(i);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest1);

    }

    private class WeatherGetter extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("https://community-open-weather-map.p.rapidapi.com/weather?%2Cuk&lat="+currentLat+"&lon="+currentLong+"&id=2172797&lang=null&units=%22metric%22")
                    .get()
                    .addHeader("x-rapidapi-key", "ffe7d32d37msh1063473921d8631p10cfbdjsnccb3d4183411")
                    .addHeader("x-rapidapi-host", "community-open-weather-map.p.rapidapi.com")
                    .build();

            try {
                okhttp3.Response response = client.newCall(request).execute();


                JSONObject responseObj = new JSONObject(response.body().string());

                JSONArray weather =  responseObj.getJSONArray("weather");
                JSONObject weatherObj = weather.getJSONObject(0);
                iconString = weatherObj.get("icon").toString();
                weatherConditions = weatherObj.get("main").toString();

                JSONObject weatherMain =  responseObj.getJSONObject("main");
                Double tempK = weatherMain.getDouble("temp");
                Double tempC = tempK - 273.15;
                DecimalFormat df = new DecimalFormat("#");

                temperature = df.format(tempC) + "\u00B0 C";
                locality = responseObj.get("name").toString();
                Log.i("Main", weatherMain.toString());


                Log.i("Weather", weather.toString());
                Log.i("Weather Obj", weatherObj.toString());


            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }



            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            TextView temp = findViewById(R.id.temp);
            TextView locale = findViewById(R.id.local);
            Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);

            ImageView icon = findViewById(R.id.weatherIcon);
            temp.setText(temperature);
            temp.startAnimation(myFadeInAnimation);
            locale.setText(locality);
            locale.startAnimation(myFadeInAnimation);
            icon.setImageBitmap(getBitmapFromURL("http://openweathermap.org/img/wn/"+iconString+"@2x.png"));
            icon.startAnimation(myFadeInAnimation);

            View divider1 = findViewById(R.id.divider1);
            divider1.setVisibility(View.VISIBLE);
            divider1.startAnimation(myFadeInAnimation);

        }
    }

        private class NewsGetter extends AsyncTask<Void, Void, Void> {

        Document doc = null;

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();


            okhttp3.Request request1 = new okhttp3.Request.Builder()
                    .url("http://newsapi.org/v2/top-headlines?country=ie&category=technology&apiKey=974d83d48a9447c184bf2ec4ff256bde")
                    .get()
                    .build();


            JSONObject json1 = null;
            try {
                okhttp3.Response response1 = client.newCall(request1).execute();
                String jsonString1 = response1.body().string();
                Log.i("API RESPONSE", jsonString1);

                JSONObject jsonObj = new JSONObject(jsonString1);

                JSONArray articles= jsonObj.getJSONArray("articles");
                int length = jsonObj.length();
                for(int i=0; i<length; i++) {
                    JSONObject article = articles.getJSONObject(i);
                    JSONObject source = article.getJSONObject("source");


                    String dateLong = article.getString("publishedAt");
                    String goodDate = dateLong.split("T")[0];
                    NewsPiece piece = new NewsPiece(article.getString("title"),article.getString("description"),article.getString("urlToImage"),goodDate,article.getString("content"),source.getString("name"), article.getString("url"));
                    Log.i("Article", piece.toString());

                        news.add(piece);


                }

                Log.i("News", news.toString());
                Log.i("News Size", String.valueOf(news.size()));



            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }





            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //This is where we update the UI with the acquired data
            NewsAdapter adapter = new NewsAdapter(news);
            RecyclerView newsView = findViewById(R.id.newsView);
            newsView.setAdapter(adapter);
            newsView.setHasFixedSize(true);
            newsView.setLayoutManager(new LinearLayoutManager(Home.this,RecyclerView.HORIZONTAL,false));
            Timer timer = new Timer();
            timer.schedule(new ScrollTask(), 0 ,5000 );


        }



    }

    private class ScrollTask extends TimerTask {
        public void run() {

            RecyclerView newsView = findViewById(R.id.newsView);
            newsView.post(new Runnable() {
                @Override
                public void run() {
                    Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                    // Call smooth scroll
                    NewsAdapter adapter = (NewsAdapter) newsView.getAdapter();

                    if(scroller<(news.size()-1)) {
                        scroller++;
                    }
                    else{
                        scroller = 0;
                    }
                    newsView.scrollToPosition(scroller);
                    newsView.startAnimation(myFadeInAnimation);
                }
            });
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {


            Intent intent = new Intent(this, Login.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {

            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.i("Bitmap","returned");
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Exception",e.getMessage());

            return null;
        }
    }
}




