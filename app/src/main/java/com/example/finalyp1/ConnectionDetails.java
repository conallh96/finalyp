package com.example.finalyp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseUser;
import com.speedchecker.android.sdk.Public.SpeedTestListener;
import com.speedchecker.android.sdk.Public.SpeedTestOptions;
import com.speedchecker.android.sdk.Public.SpeedTestResult;
import com.speedchecker.android.sdk.SpeedcheckerSDK;


import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import okhttp3.OkHttpClient;
import pl.droidsonroids.gif.GifImageView;

public class ConnectionDetails extends AppCompatActivity implements SpeedTestListener {

    private String ip;
    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private double monthlyIncome;
    private String connectType;
    private String networkName;
    private String serverLocation;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_details);

        SpeedcheckerSDK.init(this);

        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");

        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        ImageView imageView = findViewById(R.id.imageViewSpeed);

        RequestOptions options = new RequestOptions();

        Glide.with(this).load(R.drawable.blue_wifi).apply(options.circleCrop()).into(imageView);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);


        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NetworkInfo network = cm.getActiveNetworkInfo();
            Log.i("NETWORK Type", network.getTypeName());
            connectType = network.getTypeName();
            TextView net = (TextView) findViewById(R.id.ConnectionType);
            String netString = connectType;
            net.setText(netString);
        }


        SpeedcheckerSDK.SpeedTest.setOnSpeedTestListener(this);

    }





    public void pressConnection(View view){

        ImageView imageView = findViewById(R.id.imageViewSpeed);

        RequestOptions options = new RequestOptions();

        Glide.with(this).asGif().load(R.drawable.gauge_animation).apply(options.circleCrop()).into(imageView);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);

        SpeedcheckerSDK.askPermissions(this);

        SpeedcheckerSDK.SpeedTest.startTest(this);

        new IPGetter().execute();

    }

    @Override
    public void onTestStarted() {

        TextView status = (TextView) findViewById(R.id.test_status);
        status.setText("Speed Test Starting...");

    }

    @Override
    public void onFetchServerFailed() {

    }

    @Override
    public void onFindingBestServerStarted() {

    }

    @Override
    public void onTestFinished(SpeedTestResult speedTestResult) {

        TextView status = (TextView) findViewById(R.id.test_status);
        status.setText("Test Complete!");

        ImageView imageView = findViewById(R.id.imageViewSpeed);

        RequestOptions options = new RequestOptions();

        Glide.with(this).load(R.drawable.check).apply(options.circleCrop()).into(imageView);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);
    }

    @Override
    public void onPingStarted() {

        TextView status = (TextView) findViewById(R.id.test_status);
        status.setText("Testing Ping...");

    }

    @Override
    public void onPingFinished(int i) {

        TextView ping = (TextView) findViewById(R.id.Ping);
        ping.setText(i+"ms");

    }

    @Override
    public void onDownloadTestStarted() {

        TextView status = (TextView) findViewById(R.id.test_status);
        status.setText("Testing Download Speed...");

    }

    @Override
    public void onDownloadTestProgress(int i, double speedMbs, double transferredMb) {

        TextView speed = (TextView) findViewById(R.id.Down);
        speed.setText(speedMbs + " Mbps");

    }

    @Override
    public void onDownloadTestFinished(double v) {

        TextView speed = (TextView) findViewById(R.id.Down);
        speed.setText(v + " Mbps");

    }

    @Override
    public void onUploadTestStarted() {

        TextView status = (TextView) findViewById(R.id.test_status);
        status.setText("Testing Upload Speed...");

    }

    @Override
    public void onUploadTestProgress(int i, double speedMbs, double transferredMb) {
        TextView speed = (TextView) findViewById(R.id.Up);
        speed.setText(speedMbs + " Mbps");
    }

    @Override
    public void onUploadTestFinished(double v) {

        TextView speed = (TextView) findViewById(R.id.Up);
        speed.setText(v + " Mbps");

    }

    @Override
    public void onTestWarning(String s) {

    }

    @Override
    public void onTestFatalError(String s) {

    }

    @Override
    public void onTestInterrupted(String s) {

    }


    private class IPGetter extends AsyncTask<Void, Void, Void> {

        Document doc = null;

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();


            okhttp3.Request request1 = new okhttp3.Request.Builder()
                    .url("https://ip9.p.rapidapi.com/api/ip?type=json")
                    .get()
                    .addHeader("x-rapidapi-key", "ffe7d32d37msh1063473921d8631p10cfbdjsnccb3d4183411")
                    .addHeader("x-rapidapi-host", "ip9.p.rapidapi.com")
                    .build();

            JSONObject json1 = null;
            try {
                okhttp3.Response response1 = client.newCall(request1).execute();
                String jsonString1 = response1.body().string();
                Log.i("API RESPONSE", jsonString1);



                try {
                    json1 = new JSONObject(jsonString1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ip = json1.get("ip").toString();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("https://find-any-ip-address-or-domain-location-world-wide.p.rapidapi.com/iplocation?ip="+ip+"&apikey=e25d9fea442f4f70a0116c38ab803344")
                    .get()
                    .addHeader("x-rapidapi-key", "ffe7d32d37msh1063473921d8631p10cfbdjsnccb3d4183411")
                    .addHeader("x-rapidapi-host", "find-any-ip-address-or-domain-location-world-wide.p.rapidapi.com")
                    .build();

            JSONObject json = null;

            try {
                okhttp3.Response response = client.newCall(request).execute();
                String jsonString = response.body().string();
                Log.i("API RESPONSE", jsonString);



                try {
                    json = new JSONObject(jsonString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("Network", json.get("org").toString());

                networkName =  json.get("org").toString();




                serverLocation =  json.get("city").toString();





            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //This is where we update the UI with the acquired data


            TextView net = (TextView) findViewById(R.id.Network);
            net.setText(networkName);

            TextView netCity = (TextView) findViewById(R.id.Server);
            netCity.setText(serverLocation);

            TextView ipAdd = (TextView) findViewById(R.id.IP);
            String ipString =  ip;
            ipAdd.setText(ipString);


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

}