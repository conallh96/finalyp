package com.example.finalyp1;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidplot.Plot;
import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.androidplot.ui.DynamicTableModel;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class MyBreakdown extends AppCompatActivity {

    ArrayList<Utility> utilities = new ArrayList<>();
    ArrayList<String> providersList = new ArrayList<>();
    private FirebaseUser mUser;
    private String accountType;
    private BreakdownAdapter adapter = new BreakdownAdapter(utilities, providersList);
    private int userId;
    private double income;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_breakdown);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvUtils);


        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        income = intent.getDoubleExtra("Income",0.0);
        accountType = intent.getStringExtra("type");
        String response = intent.getStringExtra("Utilities");
        String provResponse = intent.getStringExtra("Providers");




        String[] utilityStrings = response.split("/");

        for (String utilityString : utilityStrings) {




            try {
                JSONObject json = new JSONObject(utilityString);

                Gson g = new Gson();
                Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);

                utilities.add(theUtil);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        String[] provStrings = provResponse.split("/");

        providersList.addAll(Arrays.asList(provStrings));




        recyclerView.setAdapter(adapter);
        recyclerView.setBackgroundColor(Color.TRANSPARENT);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyBreakdown.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(MyBreakdown.this, DividerItemDecoration.VERTICAL));


        PieChart pie = findViewById(R.id.chart);


        float radius = 26;
        pie.setBorderStyle(Plot.BorderStyle.ROUNDED,radius,radius);

        pie.setBackgroundColor(Color.TRANSPARENT);





        Random rnd = new Random();

        double total = 0;

        for (Utility util : utilities){

            total = total + util.getMonthlyPrice();

            int paint = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

            Segment segment = new Segment(util.getName()+" €" + (float) util.getMonthlyPrice(), (float) util.getMonthlyPrice());


            SegmentFormatter formatter = new SegmentFormatter(paint);



            pie.addSegment(segment, formatter);

        }




        PieChart pie2 = findViewById(R.id.chart2);

        pie2.setBorderStyle(Plot.BorderStyle.ROUNDED,radius,radius);




        Segment segment = new Segment("Income €" + (float) income, (float) income);



        SegmentFormatter formatter = new SegmentFormatter(Color.parseColor("#006400"));

        pie2.addSegment(segment, formatter);

        Segment segment2 = new Segment("Expenditure €" + (float) total, (float) total);


        SegmentFormatter formatter2 = new SegmentFormatter(Color.RED);

        pie2.addSegment(segment2, formatter2);

        recyclerView.setBackgroundColor(Color.GRAY);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }
    }
