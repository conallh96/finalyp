package com.example.finalyp1;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidplot.Plot;
import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import okhttp3.OkHttpClient;

public class PriceChecker extends AppCompatActivity {

    ArrayList<Utility> utilities = new ArrayList<>();
    ArrayList<Utility> systemUtilities = new ArrayList<>();
    ArrayList<String> providersList = new ArrayList<>();
    ArrayList<Utility> utilitiesRec = new ArrayList<>();
    ArrayList<String> providersRecList = new ArrayList<>();
    private FirebaseUser mUser;
    private BreakdownAdapter adapter = new BreakdownAdapter(utilities, providersList);
    private int userId;
    private String accountType;
    private double income;
    private String[] systemCats;
    private double totalCostInCat = 0;
    private double averageCostInCat = 0;
    private double totalUserInCat = 0;
    private double averageUserInCat = 0;
    private String responseString = "";
    private String responseString1 = "";
    private String responseString2 = "";
    private RecyclerView recyclerViewRec;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_checker);
        RecyclerView recyclerView = findViewById(R.id.rvUtils);
        recyclerViewRec = findViewById(R.id.rvUtilsRec);


        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        income = intent.getDoubleExtra("Income",0.0);
        accountType = intent.getStringExtra("type");
        String response = intent.getStringExtra("Utilities");
        String provResponse = intent.getStringExtra("Providers");


        TextView aveField = findViewById(R.id.averageField);
        TextView cheapField = findViewById(R.id.cheaperField);
        TextView cheapTxt = findViewById(R.id.cheaperText);
        TextView averageTxt = findViewById(R.id.averageText);
        TextView dealsTxt = findViewById(R.id.dealsText);
        View view = findViewById(R.id.divider3);
        view.setVisibility(View.GONE);
        dealsTxt.setVisibility(View.GONE);
        aveField.setVisibility(View.GONE);
        recyclerViewRec.setVisibility(View.GONE);
        cheapField .setVisibility(View.GONE);
        cheapTxt.setVisibility(View.GONE);
        averageTxt .setVisibility(View.GONE);






        ArrayList<String> userCategories = new ArrayList<>();

        String[] utilityStrings = response.split("/");

        for (String utilityString : utilityStrings) {




            try {
                JSONObject json = new JSONObject(utilityString);

                Gson g = new Gson();
                Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);

                utilities.add(theUtil);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        String[] provStrings = provResponse.split("/");

        providersList.addAll(Arrays.asList(provStrings));


        for (Utility util : utilities){

            if (!userCategories.contains(util.getCategory())){
                userCategories.add(util.getCategory());
            }

        }


        new SystemCats().execute();


        recyclerView.setAdapter(adapter);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(PriceChecker.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(PriceChecker.this, DividerItemDecoration.VERTICAL));


        RequestQueue queue = Volley.newRequestQueue(this);

        //Need to change to String Request

        String url = "http://localhost:8080/FYP1/rest/utilwebservice/systemutilities";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String[] utilityStrings = response.split("/");

                for (String utilityString : utilityStrings) {


                    JSONObject json = null;
                    try {
                        json = new JSONObject(utilityString);

                        Gson g = new Gson();
                        Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);


                        systemUtilities.add(theUtil);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);



    }

    public void priceCheck(View view){

        totalCostInCat = 0;
        averageCostInCat = 0;
        totalUserInCat = 0;
        averageUserInCat = 0;

        new Suggestion().execute();
        ArrayList<Utility> usersInCat = new ArrayList<>();
        ArrayList<Utility> systemInCat = new ArrayList<>();
        ArrayList<Utility> cheaperInCat = new ArrayList<>();
        ArrayList<Utility> cheapestInCat = new ArrayList<>();
        ArrayList<String> cheapestProvider = new ArrayList<>();


        Spinner spinner = findViewById(R.id.spinner2);
        String catString = spinner.getSelectedItem().toString();

        for(Utility util : utilities){

            if (util.getCategory().equals(catString)){
                usersInCat.add(util);
            }

        }

        for(Utility util : systemUtilities){

            if (util.getCategory().equals(catString)){
                systemInCat.add(util);
            }

        }

        Log.i("User's in cat", usersInCat.toString());
        Log.i("System's in cat", systemInCat.toString());
        double min = systemInCat.get(0).getMonthlyPrice();
        cheapestInCat.add(systemInCat.get(0));

        totalUserInCat = 0.0;
        totalCostInCat = 0.0;

        for(Utility util : usersInCat){

            totalUserInCat = totalUserInCat + util.getMonthlyPrice();

            for(Utility utilSys : systemInCat){



                if (utilSys.getMonthlyPrice()<util.getMonthlyPrice()){
                    cheaperInCat.add(utilSys);

                    if(utilSys.getMonthlyPrice()<min){
                        min = utilSys.getMonthlyPrice();
                        cheapestInCat.remove(0);
                        cheapestInCat.add(utilSys);
                    }

                }


            }

        }

        for(Utility utilSys : systemInCat) {
            totalCostInCat = totalCostInCat + utilSys.getMonthlyPrice();
        }

        averageCostInCat = (totalCostInCat-totalUserInCat)/(systemInCat.size()-usersInCat.size());
        averageUserInCat = totalUserInCat/usersInCat.size();

        Log.i("total system", String.valueOf(totalCostInCat));
        Log.i("total user", String.valueOf(totalUserInCat));
        Log.i("Ave Users", String.valueOf(averageUserInCat));
        Log.i("Ave Systems", String.valueOf(averageCostInCat));
        Log.i("size", String.valueOf(usersInCat.size()));

        RequestQueue queue = Volley.newRequestQueue(this);



        String url = "http://localhost:8080/FYP1/rest/providerwebservice/providerNameById/"+cheapestInCat.get(0).getProvId();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                Log.i("Provider Respone", response);
                cheapestProvider.add(response);
                Log.i("Cheapest Provider", cheapestProvider.toString());
                BreakdownAdapter adapter = new BreakdownAdapter(cheapestInCat, cheapestProvider);
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvUtils);
                recyclerView.setAdapter(adapter);
                recyclerView.startAnimation(myFadeInAnimation);



                TextView aveField = findViewById(R.id.averageField);
                TextView cheapField = findViewById(R.id.cheaperField);
                TextView cheapTxt = findViewById(R.id.cheaperText);


                TextView messageTxt = findViewById(R.id.editTextMessage);
                messageTxt.setText("Here is the cheapest deal in the category: "+ catString);
                messageTxt.startAnimation(myFadeInAnimation);

                TextView averageTxt = findViewById(R.id.averageText);


                TextView dealsTxt = findViewById(R.id.dealsText);
                dealsTxt.setVisibility(View.VISIBLE);
                dealsTxt.startAnimation(myFadeInAnimation);

                View view = findViewById(R.id.divider3);
                view.setVisibility(View.VISIBLE);




                cheapField.setVisibility(View.VISIBLE);
                cheapField.startAnimation(myFadeInAnimation);

                double difference = averageCostInCat - totalUserInCat;
                Log.i("Difference", String.valueOf(difference));
                Log.i("Ave Users", String.valueOf(averageUserInCat));
                Log.i("Ave Systems", String.valueOf(averageCostInCat));

                DecimalFormat df = new DecimalFormat("#.##");

                if (difference<0) {

                    double square = difference*-1;
                    String formatted = df.format(square);


                    String aveString = "€" + formatted;
                    aveField.setText(aveString);
                    aveField.setVisibility(View.VISIBLE);
                    aveField.startAnimation(myFadeInAnimation);

                    String message = "You pay €" + formatted + " above the average cost of products in this category";
                    averageTxt.setText(message);

                }
                else{

                    String formatted = df.format(difference);


                    String aveString = "€" + formatted;
                    aveField.setText(aveString);
                    aveField.setVisibility(View.VISIBLE);
                    aveField.startAnimation(myFadeInAnimation);

                    String message = "You pay €" + formatted + " below the average cost of products in this category";
                    averageTxt.setText(message);

                }
                averageTxt.setVisibility(View.VISIBLE);
                averageTxt.startAnimation(myFadeInAnimation);

                ArrayList<Utility> removeDupes = new ArrayList<>();

                for (Utility u : cheaperInCat) {

                    // If this element is not present in newList
                    // then add it
                    if (!removeDupes.contains(u)) {

                        removeDupes.add(u);
                    }
                }

                Utility cheapestUserInCat = usersInCat.get(0);

                for(Utility utility1 : usersInCat){
                    if (utility1.getMonthlyPrice()< cheapestUserInCat.getMonthlyPrice()){
                        cheapestUserInCat = utility1;
                    }

                }

                ArrayList<Utility> deleteCandidates = new ArrayList<>();

                for (Utility check : removeDupes){

                    if(check.getMonthlyPrice()>cheapestUserInCat.getMonthlyPrice()){
                        deleteCandidates.add(check);
                    }

                }

                for (Utility delete : deleteCandidates){

                   removeDupes.remove(delete);

                }



                String cheapString = "We found "+ removeDupes.size()+" deals at a lower price than those you have subscribed to.";
                cheapTxt.setText(cheapString);
                cheapTxt.setVisibility(View.VISIBLE);
                cheapField.setText(String.valueOf(removeDupes.size()));
                cheapTxt.startAnimation(myFadeInAnimation);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);

        Log.i("Cheaper in cat", cheaperInCat.toString());
        Log.i("Cheapest in cat", cheapestInCat.toString());







    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", income);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private class Suggestion extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/suggestCategory/"+userId)
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                responseString = response.body().string();

                okhttp3.Request request1 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/utilwebservice/utilsbycat/"+responseString)
                        .get()
                        .build();

                okhttp3.Request request2 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/utilwebservice/utilsprovsbycat/"+responseString)
                        .get()
                        .build();

                okhttp3.Response response1 = client.newCall(request1).execute();
                responseString1 = response1.body().string();

                okhttp3.Response response2 = client.newCall(request2).execute();
                responseString2 = response2.body().string();


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

        Log.i("Response0", responseString);
        Log.i("Response1", responseString1);
        Log.i("Response2", responseString2);

            String[] utilityStrings = responseString1.split("/");

            for (String utilityString : utilityStrings) {


                JSONObject json = null;
                try {
                    json = new JSONObject(utilityString);

                    Gson g = new Gson();
                    Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);


                    utilitiesRec.add(theUtil);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            String[] provStrings = responseString2.split("/");

            providersRecList.addAll(Arrays.asList(provStrings));

            UtilityAdapter adapter = new UtilityAdapter(utilitiesRec, providersRecList);

            recyclerViewRec.setAdapter(adapter);

            recyclerViewRec.setHasFixedSize(true);
            recyclerViewRec.setLayoutManager(new LinearLayoutManager(PriceChecker.this));
            recyclerViewRec.addItemDecoration(new DividerItemDecoration(PriceChecker.this, DividerItemDecoration.VERTICAL));

            recyclerViewRec.setVisibility(View.VISIBLE);


            TextView dealsTxt = findViewById(R.id.dealsText);
            dealsTxt.setText(responseString+" is a category we think you'll like, have a look!");
            dealsTxt.setVisibility(View.VISIBLE);

            Log.i("Category Utils", utilitiesRec.toString());
            Log.i("Category Providers", providersRecList.toString());

        }
    }

    private class SystemCats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/systemcategories")
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                systemCats = response.body().string().split("/");



            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            Spinner filterSpinner = findViewById(R.id.spinner2);

            ArrayList<String> common = new ArrayList<String>();

            for (String s : systemCats){
                for (Utility u : utilities ){
                    if ((s.equals(u.getCategory())&&(!common.contains(s)))){
                        common.add(s);
                    }
                }
            }

            String commons = "";

            for (String s : common){
                if (commons.equals("")){
                    commons=s;
                }
                else{
                    commons = commons + "/" + s;
                }
            }

            String[] commonCategories = commons.split("/");
            ArrayAdapter<String> adapter1;

            if (commonCategories==null) {
                adapter1 = new ArrayAdapter<>(PriceChecker.this, android.R.layout.simple_spinner_dropdown_item, systemCats);
            }
            else{
                 adapter1 = new ArrayAdapter<>(PriceChecker.this, android.R.layout.simple_spinner_dropdown_item, commonCategories);
            }

            filterSpinner.setAdapter(adapter1);


        }
    }

    }
