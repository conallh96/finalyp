package com.example.finalyp1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.androidplot.Plot;
import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import okhttp3.OkHttpClient;

public class DemoStatsResults extends AppCompatActivity {

    private FirebaseUser mUser;
    private int userId;
    private String accountType, demo, catSearch;
    private double monthlyIncome;
    private String averageSpend = "";
    private String allDemoCats = "";
    private String percentSubbed = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_stats_results);

        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");

        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");
        demo = intent.getStringExtra("demo");
        catSearch = intent.getStringExtra("cat");


        new GetDemoStats().execute();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private class GetDemoStats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/averageSpendDemo/"+demo)
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                averageSpend = response.body().string();

                okhttp3.Request request1 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/utilwebservice/catsDemo/"+demo)
                        .get()
                        .build();

                okhttp3.Request request2 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/customerwebservice/subscribedToCat/"+demo+"/"+catSearch)
                        .get()
                        .build();

                okhttp3.Response response1 = client.newCall(request1).execute();
                assert response1.body() != null;
                allDemoCats = response1.body().string();

                okhttp3.Response response2 = client.newCall(request2).execute();
                assert response2.body() != null;
                percentSubbed = response2.body().string();


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            Log.i("Response0", averageSpend);
            Log.i("Response1", allDemoCats);
            Log.i("Response2", percentSubbed);

            TextView percent = findViewById(R.id.textViewPercent);
            TextView average = findViewById(R.id.textViewAve);
            TextView percentTxt = findViewById(R.id.textViewPercentTxt);
            TextView averageTxt = findViewById(R.id.textViewAverageTxt);
            PieChart pie = findViewById(R.id.chartCats);

            try {
                double perc = Double.parseDouble(percentSubbed);
                double aver = Double.parseDouble(averageSpend);

                percent.setText(String.valueOf(perc) + "%");
                percentTxt.setText("Of Customers In This Demographic are subscribed to Utilities in the " + catSearch + " Category.");
                average.setText("€" + String.valueOf(aver));


            String categories[] = {""};

            if (allDemoCats.contains("-")) {


                categories = allDemoCats.split("-");

            }
            else {
                categories[0]=allDemoCats;
            }

            HashMap<String, Integer> popularCats = new HashMap<String,Integer>();

            for (String category : categories) {

                if (popularCats.containsKey(category)) {
                    popularCats.replace(category, popularCats.get(category) + 1);
                } else {
                    popularCats.put(category, 1);
                }

            }

            Log.i("Categories' Popularity ", popularCats.toString());




            float radius = 26;

            pie.setBorderStyle(Plot.BorderStyle.ROUNDED,radius,radius);




            CardView card1 = findViewById(R.id.cardViewPercent);
            card1.setCardElevation(0);

            CardView card2 = findViewById(R.id.cardViewAverage);
            card2.setCardElevation(0);



            Random rnd = new Random();



            for (Map.Entry<String, Integer> stringIntegerEntry : popularCats.entrySet()) {



                int paint = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

                Segment segment = new Segment(stringIntegerEntry.getKey(), stringIntegerEntry.getValue());

                SegmentFormatter formatter = new SegmentFormatter(paint);

                pie.addSegment(segment, formatter);

                Log.i("Cat", stringIntegerEntry.getKey());
                Log.i("Amount", String.valueOf(stringIntegerEntry.getValue()));

            }

            pie.invalidate();

            } catch (NumberFormatException e) {

                percent.setText("No Data");
                percentTxt.setText("Found For Demoraphic");
                average.setText("No Data");
                averageTxt.setText("Found For Demographic");

            }

        }

    }
}