package com.example.finalyp1;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.io.IOException;
import java.text.DecimalFormat;

import okhttp3.OkHttpClient;


public class EditCustomerUtilDialog extends AppCompatDialogFragment {

    private EditText utilDate;
    private EditText utilDesc;
    private EditText utilName;
    private EditText utilPrice;
    private EditText utilProvider;
    private Spinner utilCat;
    private EditCustomerUtilDialogListener listener;
    private CustomerUtil custUtil;
    private Utility utility;
    private String[] systemCats;



    public EditCustomerUtilDialog(CustomerUtil cUtil, Utility util, String[] systemCats) {
        this.custUtil = cUtil;
        this.utility = util;
        this.systemCats = systemCats;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.edit_customer_util_dialog, null);
        utilDate = view.findViewById(R.id.editUtilDate);
        utilDesc = view.findViewById(R.id.editUtilDesc);
        utilName = view.findViewById(R.id.editUtilName);
        utilProvider = view.findViewById(R.id.editUtilProvider);
        utilPrice = view.findViewById(R.id.editUtilPrice);
        utilCat = view.findViewById(R.id.spinner3);



        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, systemCats);

        utilCat.setAdapter(adapter1);



        if (custUtil != null && utility != null) {


            builder.setView(view)
                    .setTitle("Edit Details Of " + custUtil.getName())
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            listener.updateRcv();

                        }
                    })
                    .setPositiveButton("Save Changes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CustomerUtil utilEdited = custUtil;
                            Utility theUtil = utility;
                            utilEdited.setPayDate(Integer.parseInt(utilDate.getText().toString()));
                            utilEdited.setName(utilName.getText().toString());

                            theUtil.setName(utilName.getText().toString());
                            theUtil.setDescription(utilDesc.getText().toString());
                            theUtil.setCategory(utilCat.getSelectedItem().toString());
                            String provString = utilProvider.getText().toString();


                            double rawPrice = Double.parseDouble(utilPrice.getText().toString());

                            DecimalFormat df = new DecimalFormat("#.##");
                            double price = Double.parseDouble(df.format(rawPrice));

                            theUtil.setMonthlyPrice(price);

                            listener.applyTexts(utilEdited, theUtil, provString);
                        }
                    });



            String newDate = String.valueOf(custUtil.getPayDate());
            utilDate.setText(newDate);
            utilDesc.setText(utility.getDescription());
            utilPrice.setText(String.valueOf(utility.getMonthlyPrice()));
            utilName.setText(utility.getName());



        }
            return builder.create();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (EditCustomerUtilDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement EditCustomerUtilDialogListener");
        }
    }

    public interface EditCustomerUtilDialogListener {
        void applyTexts(CustomerUtil cUtil, Utility util, String providerString);
        void updateRcv();
    }


}
