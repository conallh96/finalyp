package com.example.finalyp1;


public class Provider {


	private int id;
	private String name;


	public Provider(String name) {

		this.name = name;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Provider() {

	}



}