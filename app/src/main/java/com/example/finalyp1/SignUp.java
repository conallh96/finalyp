package com.example.finalyp1;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class SignUp extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mAuth = FirebaseAuth.getInstance();
        Spinner dropdown = findViewById(R.id.countySpinner);

        String[] counties = new String[]{"County", "Antrim", "Armagh","Carlow","Cavan","Clare","Cork","Derry","Donegal","Down","Dublin","Fermanagh","Galway",
                "Kerry","Kildare","Kilkenny","Laois","Leitrim","Limerick","Longford","Louth","Mayo","Meath","Monaghan","Offaly","Roscommon","Sligo",
                "Tipperary","Tyrone","Waterford","Westmeath","Wexford","Wicklow" };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, counties);

        dropdown.setAdapter(adapter);

        ImageView imageView = (ImageView) findViewById(R.id.imageView8);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.register_icon);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);

        String[] types = new String[]{"Select Account Type", "Customer", "Business"};
        ArrayAdapter<String> adapterTypes = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, types);
        Spinner dropdownTypes = findViewById(R.id.typeSpinner);
        dropdownTypes.setAdapter(adapterTypes);
    }


    public void createUserWithEmailAndPassword(View view) {

        EditText name = (EditText) findViewById(R.id.textUsername);
        EditText pass = (EditText) findViewById(R.id.textPassword);
        EditText passConfirm = (EditText) findViewById(R.id.confirmPassword);
        EditText fullName = (EditText) findViewById(R.id.nameBox);
        EditText address1 = (EditText) findViewById(R.id.addressBox1);
        EditText address2 = (EditText) findViewById(R.id.addressBox2);
        EditText eircode = (EditText) findViewById(R.id.eircodeBox);
        EditText number = (EditText) findViewById(R.id.phoneBox);
        EditText inc = (EditText) findViewById(R.id.incomeBox);
        Spinner spinner = findViewById(R.id.countySpinner);
        Spinner spinnerType = findViewById(R.id.typeSpinner);

        if(name.getText().toString().equals("")||name.getText().toString()==null||
                pass.getText().toString().equals("")||pass.getText().toString()==null||
                passConfirm.getText().toString().equals("")||passConfirm.getText().toString()==null||
                fullName.getText().toString().equals("")||fullName.getText().toString()==null||
                address1.getText().toString().equals("")||address1.getText().toString()==null||
                address2.getText().toString().equals("")||address2.getText().toString()==null||
                eircode.getText().toString().equals("")||eircode.getText().toString()==null||
                number.getText().toString().equals("")||number.getText().toString()==null||
                inc.getText().toString().equals("")||inc.getText().toString()==null||
                spinner.getSelectedItem().toString().equals("County")||
                spinnerType.getSelectedItem().toString().equals("Select Account Type")){

            Toast.makeText(SignUp.this, "Enter Valid Details!", Toast.LENGTH_SHORT).show();

        }
        else {


            String nameString = name.getText().toString();

            String passString = pass.getText().toString();

            String confirmString = passConfirm.getText().toString();

            String fullNameString = fullName.getText().toString();

            String addressString1 = address1.getText().toString();

            String addressString2 = address2.getText().toString();

            String eircodeString = eircode.getText().toString();

            String phoneString = number.getText().toString();


            String incomeString = inc.getText().toString();
            double income = Double.parseDouble(incomeString);


            String countyString = spinner.getSelectedItem().toString();


            String typeString = spinnerType.getSelectedItem().toString();

            if (passString.matches(confirmString)) {

                mAuth.createUserWithEmailAndPassword(nameString, passString)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Log.d("myActivity", "createUserWithEmail:success");
                                    mUser = mAuth.getCurrentUser();


                                    Customer customer = new Customer(fullNameString, mUser.getEmail(), addressString1, addressString2, countyString, eircodeString, "none", phoneString, income, typeString);


                                    ObjectMapper mapper = new ObjectMapper();
                                    //Converting the Object to JSONString
                                    String jsonString = null;
                                    try {
                                        jsonString = mapper.writeValueAsString(customer);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }


                                    JSONObject json = null;
                                    try {
                                        json = new JSONObject(jsonString);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    RequestQueue queue = Volley.newRequestQueue(SignUp.this);

                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                            Request.Method.POST, "http://localhost:8080/FYP1/rest/customerwebservice/post", json, new
                                            Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {


                                                    try {
                                                        String name = response.getString("name");
                                                        String id = response.getString("id");
                                                        ;


                                                        if (response.get("type").equals("Customer")) {
                                                            Intent intent = new Intent(SignUp.this, UseCaseQuiz.class);
                                                            intent.putExtra("User", mUser);
                                                            intent.putExtra("id", Integer.valueOf(id));
                                                            intent.putExtra("Income", Double.valueOf(income));
                                                            intent.putExtra("type", response.get("type").toString());

                                                            Toast.makeText(SignUp.this, "Authentication Successful.", Toast.LENGTH_SHORT).show();

                                                            startActivity(intent);
                                                        }
                                                        if (response.get("type").equals("Business")) {
                                                            Intent intent = new Intent(SignUp.this, BusinessHome.class);
                                                            intent.putExtra("User", mUser);
                                                            intent.putExtra("id", Integer.valueOf(id));
                                                            intent.putExtra("Income", Double.valueOf(income));
                                                            intent.putExtra("type", response.get("type").toString());

                                                            Toast.makeText(SignUp.this, "Authentication Successful.", Toast.LENGTH_SHORT).show();

                                                            startActivity(intent);
                                                        }


                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {


                                            Log.i("DB Failed", error.getMessage());
                                            Toast.makeText(SignUp.this, "Authentication failed.",
                                                    Toast.LENGTH_SHORT).show();

                                        }


                                    }
                                    );

                                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    queue.add(jsonObjectRequest);


                                    //code for details

                                } else {
                                    Log.w("myActivity", "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(SignUp.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });

            } else if (!passString.matches(confirmString)) {

                Toast.makeText(SignUp.this, "Passwords Do No Match!.", Toast.LENGTH_SHORT).show();

            }
        }

    }


}