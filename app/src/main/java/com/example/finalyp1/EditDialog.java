package com.example.finalyp1;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;


public class EditDialog extends AppCompatDialogFragment {

    private EditText utilDate;
    private EditDialogListener listener;
    private CustomerUtil custUtil;
    private Utility utility;


    public EditDialog(CustomerUtil cUtil, Utility util) {
        this.custUtil = cUtil;
        this.utility = util;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.edit_dialog, null);
        utilDate = view.findViewById(R.id.editUtilDate);

        if (!(custUtil == null)) {


            builder.setView(view)
                    .setTitle("Edit Details Of " + custUtil.getName())
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            listener.updateRc();

                        }
                    })
                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CustomerUtil utilEdited = custUtil;
                            utilEdited.setPayDate(Integer.parseInt(utilDate.getText().toString()));


                            listener.applyTexts(utilEdited);
                        }
                    });



            String newDate = String.valueOf(custUtil.getPayDate());
            utilDate.setText(newDate);

        }
            return builder.create();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (EditDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement ExampleDialogListener");
        }
    }

    public interface EditDialogListener {
        void applyTexts(CustomerUtil cUtil);
        void updateRc();
    }
}
