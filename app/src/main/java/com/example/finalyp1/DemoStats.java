package com.example.finalyp1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;

import okhttp3.OkHttpClient;

public class DemoStats extends AppCompatActivity {


    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private double monthlyIncome;
    private String[] systemCats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_stats);

        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");

        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        new SystemCats().execute();
        setSpinners();
    }


    public void getStatistics(View view){


        StringBuilder builder = new StringBuilder();

        Spinner ageSpinner = findViewById(R.id.ageDemoSpinner);
        Spinner accomSpinner = findViewById(R.id.accomDemoSpinner);
        Spinner houseSpinner = findViewById(R.id.householdDemoSpinner);
        Spinner useSpinner = findViewById(R.id.usageDemoSpinner);
        Spinner countySpinner = findViewById(R.id.countyDemoSpinner);
        Spinner incomeSpinner = findViewById(R.id.incomeDemoSpinner);
        Spinner filterSpinner = findViewById(R.id.categorySpinner);

        if(ageSpinner.getSelectedItem().toString().equals("Select an Age Category")||
                accomSpinner.getSelectedItem().toString().equals("Select a Type")||
                houseSpinner.getSelectedItem().toString().equals("Select a Size")||
                countySpinner.getSelectedItem().toString().equals("County")||
                incomeSpinner.getSelectedItem().toString().equals("Income Band")||
                useSpinner.getSelectedItem().toString().equals("Use Case")){

            Toast.makeText(getApplicationContext() ,"Select Valid Options!", Toast.LENGTH_SHORT).show();

        }
        else {

            builder.append(ageSpinner.getSelectedItem().toString());


            builder.append("-" + accomSpinner.getSelectedItem().toString());


            builder.append("-" + houseSpinner.getSelectedItem().toString());


            builder.append("-" + useSpinner.getSelectedItem().toString());


            builder.append("-" + countySpinner.getSelectedItem().toString());


            builder.append("-" + incomeSpinner.getSelectedItem().toString().toLowerCase());


            String categoryOfInterest = filterSpinner.getSelectedItem().toString();


            Intent i = new Intent(getApplicationContext(), DemoStatsResults.class);

            i.putExtra("id", userId);
            i.putExtra("User", mUser);
            i.putExtra("Income", monthlyIncome);
            i.putExtra("type", accountType);
            i.putExtra("demo", builder.toString());
            i.putExtra("cat", categoryOfInterest);


            startActivity(i);

        }

    }

    public void setSpinners(){
        Spinner ageSpinner = findViewById(R.id.ageDemoSpinner);

        String[] ageItems = new String[]{"Select an Age Category", "18-29", "30-55", "55+"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, ageItems);

        ageSpinner.setAdapter(adapter);


        Spinner accomSpinner = findViewById(R.id.accomDemoSpinner);

        String[] accomItems = new String[]{"Select a Type", "House", "Apartment"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, accomItems);

        accomSpinner.setAdapter(adapter1);

        Spinner householdSpinner = findViewById(R.id.householdDemoSpinner);

        String[] householdItems = new String[]{"Select a Size", "1", "2", "3-5", "5+"};

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, householdItems);

        householdSpinner.setAdapter(adapter2);

        Spinner usageSpinner = findViewById(R.id.usageDemoSpinner);

        String[] usageItems = new String[]{"Select a Use Case", "Work From Home", "General Use", "Streaming", "Gaming"};

        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, usageItems);

        usageSpinner.setAdapter(adapter3);

        Spinner dropdown = findViewById(R.id.countyDemoSpinner);

        String[] counties = new String[]{"County", "Antrim", "Armagh","Carlow","Cavan","Clare","Cork","Derry","Donegal","Down","Dublin","Fermanagh","Galway",
                "Kerry","Kildare","Kilkenny","Laois","Leitrim","Limerick","Longford","Louth","Mayo","Meath","Monaghan","Offaly","Roscommon","Sligo",
                "Tipperary","Tyrone","Waterford","Westmeath","Wexford","Wicklow" };

        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, counties);

        dropdown.setAdapter(adapter4);

        Spinner incomeDropdown = findViewById(R.id.incomeDemoSpinner);

        String[] incomes = new String[]{"Income Band", "Low", "Medium", "High"};

        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, incomes);

        incomeDropdown.setAdapter(adapter5);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }
    private class SystemCats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/systemcategories")
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                systemCats = response.body().string().split("/");



            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            Spinner filterSpinner = findViewById(R.id.categorySpinner);

            ArrayAdapter<String> adapter1 = new ArrayAdapter<>(DemoStats.this, android.R.layout.simple_spinner_dropdown_item, systemCats);

            filterSpinner.setAdapter(adapter1);


        }
    }

}