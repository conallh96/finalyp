package com.example.finalyp1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.maps.internal.PolylineEncoding;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MapSearch extends FragmentActivity implements OnMapReadyCallback {

    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleMap mMap;
    private FirebaseUser mUser;
    private int userId;
    private double monthlyIncome;
    private String accountType;
    private double currentLat = 0.0;
    private double currentLong = 0.0;
    private Polyline line = null;

    private double selectedLat = 0.0;
    private double selectedLong = 0.0;




    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {



        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_map_search);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(MapSearch.this);



        Button directionsBttn = (Button) findViewById(R.id.directBttn);
        directionsBttn.setClickable(false);
        directionsBttn.setBackground(getDrawable(R.drawable.rounded_bttn_no_click));




        int permission = ContextCompat.checkSelfPermission(MapSearch.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {



            LocationRequest mLocationRequest;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(100000);
            mLocationRequest.setFastestInterval(50000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {

                            super.onLocationResult(locationResult);
                            currentLat = locationResult.getLastLocation().getLatitude();
                            currentLong = locationResult.getLastLocation().getLongitude();


                            LatLng here = new LatLng(currentLat, currentLong);


                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 12));


                        }
                    }
                    , null);

        }
        else {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }


    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.getUiSettings().setAllGesturesEnabled(true);

        mMap.getUiSettings().setMapToolbarEnabled(false);

        Spinner search = findViewById(R.id.mapSearchSpinner);

        String[] locations= {"ATMs", "EV Charging", "Electronic Stores"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, locations);

        search.setAdapter(adapter1);

        search.setBackgroundResource(R.drawable.spinner_background_dark);




        if (mMap != null) {
            int permission = ContextCompat.checkSelfPermission(MapSearch.this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permission == PackageManager.PERMISSION_GRANTED) {


                mMap.setMyLocationEnabled(true);


                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {

                        mMap.clear();
                        Button directionsBttn = (Button) findViewById(R.id.directBttn);
                        directionsBttn.setClickable(false);
                        directionsBttn.setBackground(getDrawable(R.drawable.rounded_bttn_no_click));
                        mMap.addMarker(new MarkerOptions().position(point).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).snippet("Clicked Location"));
                        currentLat = point.latitude;
                        currentLong = point.longitude;
                        LatLng here = new LatLng(currentLat, currentLong);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 12));

                    }
                });

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
                {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {

                        Marker marker = arg0;



                            if (marker.isInfoWindowShown()) {
                                marker.hideInfoWindow();
                                Button directionsBttn = (Button) findViewById(R.id.directBttn);
                                directionsBttn.setClickable(false);
                                directionsBttn.setBackground(getDrawable(R.drawable.rounded_bttn_no_click));


                            } else {

                                marker.showInfoWindow();
                                selectedLat = marker.getPosition().latitude;
                                selectedLong = marker.getPosition().longitude;
                                Button directionsBttn = (Button) findViewById(R.id.directBttn);
                                directionsBttn.setClickable(true);
                                directionsBttn.setBackground(getDrawable(R.drawable.rounded_bttn));


                            }


                        return true;
                    }
                });



            } else {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        0);
            }


        }

    }

    public void searchClick(View view){

        Spinner search = findViewById(R.id.mapSearchSpinner);

        String searchString = search.getSelectedItem().toString();

        switch(searchString){

            case "ATMs":
                AtmSearch();
                break;

            case "EV Charging":
                EVSearch();
                break;

            case "Electronic Stores":
                nearbySearch();


        }

    }

    public void EVSearch(){

        mMap.clear();

        LatLng here = new LatLng(currentLat, currentLong);
        mMap.addMarker(new MarkerOptions().position(here).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).snippet("Origin"));

        RequestQueue queue = Volley.newRequestQueue(this);

        Log.i("EV Clicked: ", "Loading...");



        String url = "https://api.openchargemap.io/v3/poi/?output=json&countrycode=IE&maxresults=10&compact=true&key=dda61559-5c1f-431d-8d8a-1c48e5cdd76c&verbose=false&latitude="+currentLat+"&longitude="+currentLong+"&distance=10";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("EV Charging Response: ", response);

                JSONArray results = null;
                try {
                    results = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                assert results != null;
                Log.i("EV Charging Array: ", results.toString());


                for (int i = 0; i < results.length(); i ++) {

                    try {
                        JSONObject result = results.getJSONObject(i);
                        JSONObject geometry = result.getJSONObject("AddressInfo");
                        String latString = geometry.getString("Latitude");
                        double lat = Double.parseDouble(latString);
                        String lngString = geometry.getString("Longitude");
                        double lng = Double.parseDouble(lngString);
                        LatLng here = new LatLng(lat, lng);



                        mMap.addMarker(new MarkerOptions().position(here).title(geometry.getString("Title")).snippet(geometry.getString("AddressLine1")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }








                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("EV Charging Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);


    }

    public void getDirections(View view){

        RequestQueue queue = Volley.newRequestQueue(this);

        Log.i("Directions Button", "Clicked");

        String str_origin = "origin=" + currentLat + "," + currentLong;
        // Destination of route
        String str_dest = "destination=" + selectedLat + "," + selectedLong;
        // Mode
        String mode = "mode=" + "driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=AIzaSyB2AcydYAWt8678unUusfQWSmyFUkugEgs";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url , null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i("Directions Response", response.toString());
                        List<com.google.maps.model.LatLng> list = null;

                        try {



                        JSONArray routeArray = null;

                        routeArray = response.getJSONArray("routes");

                        JSONObject routes = routeArray.getJSONObject(0);
                        JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                        String encodedString = overviewPolylines.getString("points");
                        list = PolylineEncoding.decode(encodedString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        assert list != null;
                        Log.i("List", list.toString());



                        if (line != null){
                            line.remove();
                        }

                        PolylineOptions options = new PolylineOptions().width(10).color(Color.BLUE).geodesic(true);
                        for (int i = 0; i < list.size(); i++) {
                            com.google.maps.model.LatLng point = list.get(i);
                            LatLng point1 = new LatLng(point.lat,point.lng);

                            options.add(point1);
                        }
                        mMap.clear();
                        LatLng here = new LatLng(currentLat, currentLong);
                        LatLng there = new LatLng(selectedLat, selectedLong);
                        mMap.addMarker(new MarkerOptions().position(here).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("Origin"));
                        mMap.addMarker(new MarkerOptions().position(there).title("Destination"));
                        line = mMap.addPolyline(options);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {



            }


        }
        );

        queue.add(jsonObjectRequest);

    }

    public void nearbySearch() {

        mMap.clear();
        LatLng here = new LatLng(currentLat, currentLong);
        mMap.addMarker(new MarkerOptions().position(here).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).snippet("Origin"));

        RequestQueue queue = Volley.newRequestQueue(this);




        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, "https://maps.googleapis.com/maps/api/place/search/json?location="+currentLat+","+currentLong+"&radius=2500&type=electronics_store&sensor=true&key=AIzaSyB2AcydYAWt8678unUusfQWSmyFUkugEgs", null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                        try {

                            JSONArray cafes = response.getJSONArray("results");
                            for (int i = 0; i < cafes.length(); i ++) {

                                JSONObject cafe = cafes.getJSONObject(i);

                                JSONObject geometry = cafe.getJSONObject("geometry");
                                JSONObject locale = geometry.getJSONObject("location");


                                String latString = locale.getString("lat");
                                double lat = Double.parseDouble(latString);
                                String lngString = locale.getString("lng");
                                double lng = Double.parseDouble(lngString);
                                LatLng here1 = new LatLng(lat, lng);



                                    mMap.addMarker(new MarkerOptions().position(here1).title(cafe.getString("name")).snippet(cafe.getString("vicinity")));



                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {



            }


        }
        );

        queue.add(jsonObjectRequest);




        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 12));


    }

    public void AtmSearch() {

        mMap.clear();
        LatLng here = new LatLng(currentLat, currentLong);
        mMap.addMarker(new MarkerOptions().position(here).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).snippet("Origin"));

        RequestQueue queue = Volley.newRequestQueue(this);




        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, "https://maps.googleapis.com/maps/api/place/search/json?location="+currentLat+","+currentLong+"&radius=2500&type=atm&sensor=true&key=AIzaSyB2AcydYAWt8678unUusfQWSmyFUkugEgs", null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                        try {

                            JSONArray cafes = response.getJSONArray("results");
                            for (int i = 0; i < cafes.length(); i ++) {

                                JSONObject cafe = cafes.getJSONObject(i);

                                JSONObject geometry = cafe.getJSONObject("geometry");
                                JSONObject locale = geometry.getJSONObject("location");


                                String latString = locale.getString("lat");
                                double lat = Double.parseDouble(latString);
                                String lngString = locale.getString("lng");
                                double lng = Double.parseDouble(lngString);
                                LatLng here1 = new LatLng(lat, lng);



                                mMap.addMarker(new MarkerOptions().position(here1).title(cafe.getString("name")).snippet(cafe.getString("vicinity")));



                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {



            }


        }
        );

        queue.add(jsonObjectRequest);




        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 12));


    }

    public void goHome(View view){

        if (accountType.equals("Customer")) {
            Intent intent = new Intent(getApplicationContext(), Home.class);

            intent.putExtra("User", mUser);

            intent.putExtra("Income", monthlyIncome);

            intent.putExtra("id", userId);

            intent.putExtra("type", accountType);

            startActivity(intent);
        }

        if (accountType.equals("Business")) {
            Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

            intent.putExtra("User", mUser);

            intent.putExtra("Income", monthlyIncome);

            intent.putExtra("id", userId);

            intent.putExtra("type", accountType);

            startActivity(intent);
        }


    }


}