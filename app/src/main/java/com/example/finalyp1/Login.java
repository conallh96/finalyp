package com.example.finalyp1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;



public class Login extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
    }


    public void createUserWithEmailAndPassword(View view) {

        EditText name = (EditText) findViewById(R.id.textUsername);
        String nameString = name.getText().toString();

        EditText pass = (EditText) findViewById(R.id.textPassword);
        String passString = pass.getText().toString();

        mAuth.createUserWithEmailAndPassword(nameString, passString)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("myActivity", "createUserWithEmail:success");
                            mUser = mAuth.getCurrentUser();

                            //code for details

                        } else {
                            Log.w("myActivity", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }

                    }

                });
    }

    public void signInWithEmailAndPassword(View view) {

        EditText name = (EditText) findViewById(R.id.textUsername);
        String nameString = name.getText().toString();

        EditText pass = (EditText) findViewById(R.id.textPassword);
        String passString = pass.getText().toString();


        mAuth.signInWithEmailAndPassword(nameString, passString)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult>task) {

                        if (task.isSuccessful()) {


                            Log.i("LOGIN MESSAGE", "SUCCESS");


                            mUser = mAuth.getCurrentUser();

                            RequestQueue queue = Volley.newRequestQueue(Login.this);

                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                    Request.Method.GET, "http://localhost:8080/FYP1/rest/customerwebservice/customerEmail/"+mUser.getEmail(), null, new
                                    Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {



                                            try {
                                                String name = response.getString("name");
                                                String id = response.getString("id");


                                                if(response.get("type").equals("Customer")) {
                                                    Intent intent = new Intent(Login.this, Home.class);

                                                    String income = response.getString("monthlyIncome");
                                                    intent.putExtra("Name", name);
                                                    intent.putExtra("Income", Double.parseDouble(income));
                                                    intent.putExtra("User", mUser);
                                                    intent.putExtra("id", Integer.valueOf(id));
                                                    intent.putExtra("type", response.get("type").toString());
                                                    startActivity(intent);
                                                }
                                                if(response.get("type").equals("Business")){
                                                    Intent intent = new Intent(Login.this, BusinessHome.class);
                                                    String income = response.getString("monthlyIncome");
                                                    intent.putExtra("Name", name);
                                                    intent.putExtra("Income", Double.parseDouble(income));
                                                    intent.putExtra("User", mUser);
                                                    intent.putExtra("id", Integer.valueOf(id));
                                                    intent.putExtra("type", response.get("type").toString());
                                                    startActivity(intent);
                                                }
                                                Toast.makeText(Login.this, "User signed in", Toast.LENGTH_LONG).show();



                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {


                                    Toast.makeText(Login.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }


                            }
                            );

                            queue.add(jsonObjectRequest);






                        } else {
                            Log.w("MySignin", "SignInUserWithEmail:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void signUp(View view){

        Intent intent = new Intent(Login.this, SignUp.class);

        startActivity(intent);

    }


}