package com.example.finalyp1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;

public class AddUtil extends AppCompatActivity {

    private FirebaseUser mUser;
    String providerId;
    private String accountType;
    private int customerId;
    private double monthlyIncome;
    private String[] systemCats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_util);

        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        customerId = intent.getIntExtra("id", 0);
        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        accountType = intent.getStringExtra("type");

        new SystemCats().execute();

        ImageView imageView = (ImageView) findViewById(R.id.imageView2);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.small_util);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);

    }

    public void addUtil(View view){

        Spinner spinner = findViewById(R.id.spinner);
        String catString = spinner.getSelectedItem().toString();

        if (catString.equals("Select a Category")|| spinner.getSelectedItem() == null){

            Toast.makeText(AddUtil.this, "Please Select a Category!", Toast.LENGTH_SHORT).show();

        }
        else {

            EditText nameBox = (EditText) findViewById(R.id.utilName);
            EditText descBox = (EditText) findViewById(R.id.utilDescription);
            EditText priceBox = (EditText) findViewById(R.id.utilPrice);
            EditText provBox = (EditText) findViewById(R.id.providerName);
            EditText dateBox = (EditText) findViewById(R.id.utilDate);

            if (nameBox.getText().toString().equals("") || nameBox.getText().toString() == null ||
                    descBox.getText().toString().equals("") || descBox.getText().toString() == null ||
                    priceBox.getText().toString().equals("") || priceBox.getText().toString() == null ||
                    provBox.getText().toString().equals("") || provBox.getText().toString() == null ||
                    dateBox.getText().toString().equals("") || dateBox.getText().toString() == null) {

                Toast.makeText(AddUtil.this, "Enter Valid Details!", Toast.LENGTH_SHORT).show();

            } else {


                String name = nameBox.getText().toString();
                String description = descBox.getText().toString();
                Double cost = Double.valueOf(priceBox.getText().toString());
                String providerName = provBox.getText().toString();
                String dateString = dateBox.getText().toString();

                int date = Integer.parseInt(dateString);


                // Check DB for provider

                Provider provider = new Provider(providerName);

                ObjectMapper mapper = new ObjectMapper();
                //Converting the Object to JSONString
                String jsonString = null;
                try {
                    jsonString = mapper.writeValueAsString(provider);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ;

                JSONObject jsonProvider = null;
                try {
                    jsonProvider = new JSONObject(jsonString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                RequestQueue queue = Volley.newRequestQueue(AddUtil.this);


                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, "http://localhost:8080/FYP1/rest/providerwebservice/post", jsonProvider, new
                        Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {


                                try {
                                    providerId = response.getString("id");

                                    Log.i("Success", providerId + " is the Id of " + providerName);

                                    int provInt = Integer.parseInt(providerId);

                                    // Create Utility Object and Parse to Json
                                    Utility util = new Utility(cost, name, description, provInt, "User", catString);


                                    String jsonStringUtil = null;
                                    try {
                                        jsonStringUtil = mapper.writeValueAsString(util);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    ;

                                    JSONObject jsonUtil = null;
                                    try {
                                        jsonUtil = new JSONObject(jsonStringUtil);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    JsonObjectRequest jsonObjectRequest1 = new JsonObjectRequest(
                                            Request.Method.POST, "http://localhost:8080/FYP1/rest/utilwebservice/post", jsonUtil, new
                                            Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {


                                                    try {
                                                        String added = response.getString("name");
                                                        String utilId = response.getString("id");
                                                        Toast.makeText(getApplicationContext(), added + " Added to Account!", Toast.LENGTH_SHORT).show();

                                                        nameBox.setText("");
                                                        dateBox.setText("");
                                                        descBox.setText("");
                                                        priceBox.setText("");
                                                        provBox.setText("");
                                                        Log.i("Successfully Added: ", added);

                                                        int utilityId = Integer.parseInt(utilId);
                                                        int custId = customerId;

                                                        CustomerUtil custUtil = new CustomerUtil(name, custId, utilityId, date);
                                                        String jsonStringCustUtil = null;
                                                        try {
                                                            jsonStringCustUtil = mapper.writeValueAsString(custUtil);
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                        ;

                                                        JSONObject jsonCustUtil = null;
                                                        try {
                                                            jsonCustUtil = new JSONObject(jsonStringCustUtil);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }


                                                        //Jersey POST to Add Customer Utility to DB

                                                        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(
                                                                Request.Method.POST, "http://localhost:8080/FYP1/rest/customerutilwebservice/post", jsonCustUtil, new
                                                                Response.Listener<JSONObject>() {
                                                                    @Override
                                                                    public void onResponse(JSONObject response) {


                                                                        try {
                                                                            String name = response.getString("name");
                                                                            Log.i("Success", name + " added to CustomerUtil in DB");
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }


                                                                    }
                                                                }, new Response.ErrorListener() {

                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {


                                                                Log.i("DB Failed", error.getMessage());

                                                            }


                                                        }
                                                        );

                                                        jsonObjectRequest2.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                        queue.add(jsonObjectRequest2);


                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {


                                            Log.i("DB Failed", error.getMessage());

                                        }


                                    }
                                    );

                                    jsonObjectRequest1.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    queue.add(jsonObjectRequest1);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.i("DB Failed", error.getMessage());

                    }


                }
                );

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsonObjectRequest);

            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private class SystemCats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/systemcategories")
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                systemCats = response.body().string().split("/");



            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            Spinner filterSpinner = findViewById(R.id.spinner);

            ArrayAdapter<String> adapter1 = new ArrayAdapter<>(AddUtil.this, android.R.layout.simple_spinner_dropdown_item, systemCats);

            filterSpinner.setAdapter(adapter1);


        }
    }
}