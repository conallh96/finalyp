package com.example.finalyp1;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

public class ViewDeals extends AppCompatActivity {

    ArrayList<Utility> utilities = new ArrayList<>();
    ArrayList<String> providersList = new ArrayList<>();
    ArrayList<Utility> utilitiesDisplayed = new ArrayList<>();
    ArrayList<String> providersDisplayed = new ArrayList<>();
    private FirebaseUser mUser;
    private double monthlyIncome;
    private String accountType;
    private UtilityAdapter adapter = new UtilityAdapter(utilitiesDisplayed, providersDisplayed);
    private int userId;
    private String[] systemCats;
    Utility subbed = new Utility();
    private String responseString1,responseString2,catString;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_utils);

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvUtils);

        ImageView imageView = (ImageView) findViewById(R.id.imageView4);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mobile_phone);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);

        Animation myFadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        imageView.startAnimation(myFadeInAnimation);

        Intent intent = getIntent();
        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        accountType = intent.getStringExtra("type");
        userId = intent.getIntExtra("id", 0);
        mUser = intent.getParcelableExtra("User");
        String response = intent.getStringExtra("Response");


        String[] utilityStrings = response.split("/");

        for (String utilityString : utilityStrings) {


            JSONObject json = null;
            try {
                json = new JSONObject(utilityString);

                Gson g = new Gson();
                Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);


                utilities.add(theUtil);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        new SystemCats().execute();

        String url = "http://localhost:8080/FYP1/rest/utilwebservice/systemutilityprovs";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                String[] provStrings = response.split("/");

                providersList.addAll(Arrays.asList(provStrings));


                Log.i("Utilities Array", utilities.toString());

                utilitiesDisplayed.addAll(utilities);
                providersDisplayed.addAll(providersList);
                recyclerView.setAdapter(adapter);

                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(ViewDeals.this));
                recyclerView.addItemDecoration(new DividerItemDecoration(ViewDeals.this, DividerItemDecoration.VERTICAL));



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("DB Failed", error.getMessage());

            }


        });

        queue.add(stringRequest);

    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            if (accountType.equals("Customer")) {
                subbed = utilitiesDisplayed.get(viewHolder.getAdapterPosition());

                AlertDialog.Builder builder1 = new AlertDialog.Builder(viewHolder.itemView.getContext());
                builder1.setTitle("Subscribe to " + subbed.getName());
                builder1.setMessage("Are you sure you want to subscribe to " + subbed.getName() + "?\n\n We will inform the Service Provider of your " +
                        "interest and they will be in touch to complete your purchase and installation.\n\n");
                builder1.setCancelable(true);


                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                adapter.notifyDataSetChanged();

                                Calendar cal = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd");
                                String day = sdf.format(cal.getTime());
                                int date = Integer.parseInt(day);

                                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

                                CustomerUtil custUtil = new CustomerUtil(subbed.getName(), userId, subbed.getId(), date);
                                String jsonStringCustUtil = null;
                                ObjectMapper mapper = new ObjectMapper();
                                try {

                                    jsonStringCustUtil = mapper.writeValueAsString(custUtil);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                ;

                                JSONObject jsonCustUtil = null;
                                try {
                                    jsonCustUtil = new JSONObject(jsonStringCustUtil);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                //Jersey POST to Add Customer Utility to DB

                                JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(
                                        Request.Method.POST, "http://localhost:8080/FYP1/rest/customerutilwebservice/post", jsonCustUtil, new
                                        Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {


                                                try {
                                                    new EmailSender().execute();
                                                    new RequestSender().execute();
                                                    String name = response.getString("name");
                                                    Log.i("Success", name + " added to CustomerUtil in DB");
                                                    Toast.makeText(ViewDeals.this, "Subscription Requested Successfully!", Toast.LENGTH_SHORT).show();

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }


                                            }
                                        }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {


                                        Log.i("DB Failed", error.getMessage());

                                    }


                                }
                                );

                                jsonObjectRequest2.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                queue.add(jsonObjectRequest2);


                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                adapter.notifyDataSetChanged();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
            else{
                Toast.makeText(ViewDeals.this, "Business Users Cannot Subscribe!", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }
        }

    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public void filter(View view){

        Spinner filterSpinner = findViewById(R.id.filterSpinner);
        catString = filterSpinner.getSelectedItem().toString();
        new FilterResults().execute();

    }

    private class FilterResults extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request1 = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/utilsbycat/"+catString)
                    .get()
                    .build();

            okhttp3.Request request2 = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/utilsprovsbycat/"+catString)
                    .get()
                    .build();



            try {
                okhttp3.Response response1 = client.newCall(request1).execute();
                responseString1 = response1.body().string();

                okhttp3.Response response2 = client.newCall(request2).execute();
                responseString2 = response2.body().string();


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            utilitiesDisplayed.clear();
            providersDisplayed.clear();
            Log.i("Response1", responseString1);
            Log.i("Response2", responseString2);

            String[] utilityStrings = responseString1.split("/");

            for (String utilityString : utilityStrings) {


                JSONObject json = null;
                try {
                    json = new JSONObject(utilityString);

                    Gson g = new Gson();
                    Utility theUtil = g.fromJson(String.valueOf(json), Utility.class);


                    utilitiesDisplayed.add(theUtil);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            String[] provStrings = responseString2.split("/");

            providersDisplayed.addAll(Arrays.asList(provStrings));



            adapter.notifyDataSetChanged();


            Log.i("Category Utils", utilitiesDisplayed.toString());
            Log.i("Category Providers", providersDisplayed.toString());

        }
    }

    private class EmailSender extends AsyncTask<Void, Void, Void> {



        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType,"{\n    \"personalizations\": [\n        {\n            \"to\": [\n                {\n                    \"email\": \""+mUser.getEmail()+"\"\n                }\n            ],\n            \"subject\": \"Subscription Request Confirmed!\"\n        }\n    ],\n    \"from\": {\n        \"email\": \"digitools.subscribed@gmail.com\"\n    },\n    \"content\": [\n        {\n            \"type\": \"text/plain\",\n            \"value\": \"Hello, thank you for your service. You have notified the Provider of your request to subscribe to "+subbed.getName()+".\"\n        }\n    ]\n}");
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("https://rapidprod-sendgrid-v1.p.rapidapi.com/mail/send")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("x-rapidapi-key", "ffe7d32d37msh1063473921d8631p10cfbdjsnccb3d4183411")
                    .addHeader("x-rapidapi-host", "rapidprod-sendgrid-v1.p.rapidapi.com")
                    .build();

            try {
                okhttp3.Response response = client.newCall(request).execute();
                String jsonString1 = response.message();
                Log.i("API RESPONSE", jsonString1);
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            //This is where we update the UI with the acquired data




        }
    }




    private class RequestSender extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();



            MediaType mediaType = MediaType.parse("application/json");
            okhttp3.RequestBody body1 = okhttp3.RequestBody.create(mediaType,"{\n    \"personalizations\": [\n        {\n            \"to\": [\n                {\n                    \"email\": \"digitools.subscribed@gmail.com\"\n                }\n            ],\n            \"subject\": \"Subscription Request Submitted!\"\n        }\n    ],\n    \"from\": {\n        \"email\": \""+mUser.getEmail()+"\"\n    },\n    \"content\": [\n        {\n            \"type\": \"text/plain\",\n            \"value\": \"Hello, "+mUser.getEmail()+" has requested to subscribe to "+subbed.getName()+", with the ID of "+subbed.getId()+".\"\n        }\n    ]\n}");
            okhttp3.Request request1 = new okhttp3.Request.Builder()
                    .url("https://rapidprod-sendgrid-v1.p.rapidapi.com/mail/send")
                    .post(body1)
                    .addHeader("content-type", "application/json")
                    .addHeader("x-rapidapi-key", "ffe7d32d37msh1063473921d8631p10cfbdjsnccb3d4183411")
                    .addHeader("x-rapidapi-host", "rapidprod-sendgrid-v1.p.rapidapi.com")
                    .build();

            try {
                okhttp3.Response response1 = client.newCall(request1).execute();
                String jsonString2 = response1.message();
                Log.i("API RESPONSE SEC EMAIL", jsonString2);
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            //This is where we update the UI with the acquired data




        }
    }

    private class SystemCats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/systemcategories")
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                systemCats = response.body().string().split("/");



            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            Spinner filterSpinner = findViewById(R.id.filterSpinner);

            ArrayAdapter<String> adapter1 = new ArrayAdapter<>(ViewDeals.this, android.R.layout.simple_spinner_dropdown_item, systemCats);

            filterSpinner.setAdapter(adapter1);


        }
    }

}
