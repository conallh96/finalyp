package com.example.finalyp1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.androidplot.Plot;
import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import okhttp3.OkHttpClient;

public class MarketSnapShot extends AppCompatActivity {


    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private double monthlyIncome;
    private Utility mostPop;
    private String[] demo;
    private String[] players;
    private String[] percents;
    private DemographicCount biggestDemo;
    private String percentSubbed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_snap_shot);

        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");

        monthlyIncome = intent.getDoubleExtra("Income", 0.0);
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        new GetMarketStats().execute();
    }


    private class GetMarketStats extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/FYP1/rest/utilwebservice/mostPopUtil")
                    .get()
                    .build();



            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();



                okhttp3.Request request2 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/customerwebservice/biggestDemo")
                        .get()
                        .build();


                okhttp3.Response response2 = client.newCall(request2).execute();
                assert response2.body() != null;
                String result2 = response2.body().string();

                Log.i("Response1", result);
                Log.i("Response2", result2);

                ObjectMapper mapper = new ObjectMapper();
                mostPop = mapper.readValue(result, Utility.class);
                biggestDemo = mapper.readValue(result2, DemographicCount.class);
                demo = biggestDemo.getName().split("-");

                okhttp3.Request request3 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/utilwebservice/subscribedToUtil/"+mostPop.getId())
                        .get()
                        .build();

                okhttp3.Response response3 = client.newCall(request3).execute();
                assert response3.body() != null;
                percentSubbed = response3.body().string();
                double subbed = Double.parseDouble(percentSubbed);
                subbed = Math.rint(subbed);
                percentSubbed = String.valueOf(subbed);





                okhttp3.Request request4 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/customerwebservice/marketShares")
                        .get()
                        .build();

                okhttp3.Response response4 = client.newCall(request4).execute();
                assert response4.body() != null;
                percents = response4.body().string().split("-");

                okhttp3.Request request5 = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/FYP1/rest/customerwebservice/marketPlayers")
                        .get()
                        .build();

                okhttp3.Response response5 = client.newCall(request5).execute();
                assert response5.body() != null;
                players = response5.body().string().split("-");


                Log.i("Players", players[0]);
                Log.i("Percents", percents[0]);



            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            TextView name = findViewById(R.id.namePop);
            TextView category = findViewById(R.id.categoryPop);
            TextView price = findViewById(R.id.pricePop);
            TextView age = findViewById(R.id.demoAge);
            TextView county = findViewById(R.id.demoCounty);
            TextView housing = findViewById(R.id.demoHousing);
            TextView size = findViewById(R.id.demoSize);
            TextView subbed = findViewById(R.id.percentSubbed);

            name.setText(mostPop.getName());
            category.setText(mostPop.getCategory());
            price.setText("€"+String.valueOf(mostPop.getMonthlyPrice()));

            String ageCat = "Age Range: " + demo[0]+"-"+demo[1];
            age.setText(ageCat);
            county.setText("County: " + demo[5]);
            housing.setText("Accommodation: " + demo[2]);
            size.setText("Demo Size: " + biggestDemo.getSize());
            subbed.setText(percentSubbed+"% of Customers are subscribed.");

            BarChart barChart = (BarChart) findViewById(R.id.pieSharechart);

            ArrayList<BarEntry> entries = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();
            for(int i = 0; i < percents.length; i++) {
                entries.add(new BarEntry((Float.parseFloat(percents[i])), i));
                labels.add(players[i]);
            }


            BarDataSet bardataset = new BarDataSet(entries, "Market Share (%)");
            BarData data = new BarData(labels, bardataset);
            barChart.setDescription("");
            barChart.setDrawValueAboveBar(false);
            barChart.setData(data);
            bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
            barChart.animateY(5000);
            barChart.setBackgroundColor(Color.WHITE);
            barChart.getLegend().setEnabled(true);





        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", userId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

}