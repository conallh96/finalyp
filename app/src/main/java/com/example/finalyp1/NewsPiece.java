package com.example.finalyp1;

public class NewsPiece {

    private String title;
    private String description;
    private String imageUrl;
    private String date;
    private String content;
    private String sourceName;
    private String url;

    public NewsPiece(){

    }

    public NewsPiece(String title, String description, String imageUrl, String date, String content, String sourceName, String url) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.date = date;
        this.content = content;
        this.sourceName = sourceName;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    @Override
    public String toString() {
        return "NewsPiece{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", date='" + date + '\'' +
                ", content='" + content + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
