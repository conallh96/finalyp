package com.example.finalyp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class UseCaseQuiz extends AppCompatActivity {

    private FirebaseUser mUser;
    private int customerId;
    private double monthlyIncome;
    private Customer customer;
    private String accountType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_case_quiz);
        Intent intent = getIntent();
        mUser = intent.getParcelableExtra("User");
        customerId = intent.getIntExtra("id", 0);
        monthlyIncome = intent.getDoubleExtra("Income",0.0);
        accountType = intent.getStringExtra("type");
        setSpinners();



        RequestQueue queue = Volley.newRequestQueue(UseCaseQuiz.this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, "http://localhost:8080/FYP1/rest/customerwebservice/customerId/"+customerId, null, new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                        ObjectMapper objectMapper = new ObjectMapper();
                        try {
                            customer = objectMapper.readValue(response.toString(), Customer.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Log.i("Customer", customer.toString());

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(UseCaseQuiz.this, "Getting User Details failed.",
                        Toast.LENGTH_SHORT).show();

            }


        }
        );

        queue.add(jsonObjectRequest);




    }

    public void updateUseCase(View view){

        StringBuilder builder = new StringBuilder();

        Spinner ageSpinner = findViewById(R.id.ageSpinner);
        Spinner accomSpinner = findViewById(R.id.accomSpinner);
        Spinner houseSpinner = findViewById(R.id.householdSpinner);
        Spinner useSpinner = findViewById(R.id.usageSpinner);


        if(ageSpinner.getSelectedItem().toString().equals("Category")||
                accomSpinner.getSelectedItem().toString().equals("Type")||
                houseSpinner.getSelectedItem().toString().equals("Size")||
                useSpinner.getSelectedItem().toString().equals("Use Case")){

            Toast.makeText(getApplicationContext() ,"Select Valid Options!", Toast.LENGTH_SHORT).show();

        }
        else {


            builder.append(ageSpinner.getSelectedItem().toString());


            builder.append("-" + accomSpinner.getSelectedItem().toString());


            builder.append("-" + houseSpinner.getSelectedItem().toString());


            builder.append("-" + useSpinner.getSelectedItem().toString());

            builder.append("-" + customer.getCounty());

            if (monthlyIncome <= 2500) {
                builder.append("-low");
            } else if (monthlyIncome > 2500 && monthlyIncome <= 5000) {
                builder.append("-medium");
            } else {
                builder.append("-high");
            }


            customer.setDemographic(builder.toString());


            ObjectMapper mapper = new ObjectMapper();
            //Converting the Object to JSONString
            String jsonString = null;
            try {
                jsonString = mapper.writeValueAsString(customer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ;

            JSONObject json = null;
            try {
                json = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Jersey PUT with customer JSON and Users ID;

            RequestQueue queue = Volley.newRequestQueue(UseCaseQuiz.this);


            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.PUT, "http://localhost:8080/FYP1/rest/customerwebservice/update/" + customerId, json, new
                    Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {


                            try {
                                String name = response.getString("name");
                                Log.i("Success", name + " updates in DB");
                                Toast.makeText(getApplicationContext(), "Your Use Case Has Been Updated!", Toast.LENGTH_LONG).show();

                                if (accountType.equals("Customer")) {
                                    Intent intent = new Intent(getApplicationContext(), Home.class);

                                    intent.putExtra("User", mUser);

                                    intent.putExtra("Income", monthlyIncome);

                                    intent.putExtra("id", customerId);

                                    intent.putExtra("type", accountType);

                                    startActivity(intent);
                                }

                                if (accountType.equals("Business")) {
                                    Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                                    intent.putExtra("User", mUser);

                                    intent.putExtra("Income", monthlyIncome);

                                    intent.putExtra("id", customerId);

                                    intent.putExtra("type", accountType);

                                    startActivity(intent);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {


                    Log.i("DB Failed", error.getMessage());

                }


            }
            );


            queue.add(jsonObjectRequest);

        }

    }

    public void setSpinners(){
        Spinner ageSpinner = findViewById(R.id.ageSpinner);

        String[] ageItems = new String[]{"Category", "18-29", "30-55", "55+"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, ageItems);

        ageSpinner.setAdapter(adapter);


        Spinner accomSpinner = findViewById(R.id.accomSpinner);

        String[] accomItems = new String[]{"Type", "House", "Apartment"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, accomItems);

        accomSpinner.setAdapter(adapter1);

        Spinner householdSpinner = findViewById(R.id.householdSpinner);

        String[] householdItems = new String[]{"Size", "1", "2", "3-5", "5+"};

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, householdItems);

        householdSpinner.setAdapter(adapter2);

        Spinner usageSpinner = findViewById(R.id.usageSpinner);

        String[] usageItems = new String[]{"Use Case", "Work From Home", "General Use", "Streaming", "Gaming"};

        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, usageItems);

        usageSpinner.setAdapter(adapter3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }
        if (item.getItemId() == R.id.settings) {


            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Settings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), AdminSettings.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.home) {
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), Home.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

            if (accountType.equals("Business")) {
                Intent intent = new Intent(getApplicationContext(), BusinessHome.class);

                intent.putExtra("User", mUser);

                intent.putExtra("Income", monthlyIncome);

                intent.putExtra("id", customerId);

                intent.putExtra("type", accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

}